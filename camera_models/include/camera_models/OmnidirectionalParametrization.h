#ifndef OMNIDIRECTIONALPARAMETRIZATION_H
#define OMNIDIRECTIONALPARAMETRIZATION_H
#include "camera_models/CameraModel.h"
#include <Eigen/Eigen>
#include <ceres/ceres.h>

struct OmnidirectionalCalibrationParams : public CameraCalibrationParams {
  bool fixedStretch = false;
  bool identStretch = false;
  bool diagonalStretch = false;
  bool fixedPrincipal = false;
  bool equalPrincipal = false;
  bool fixedPolynom = false;
  bool fixEven = false;
  bool fixOdd = false;
  int rN = 5;
  int lS, gS;
  bool isfixed() {
    bool fixPoly = fixedPolynom || (fixEven && fixOdd);
    bool fixSt = fixedStretch || identStretch;
    return fixedPrincipal && fixSt && fixPoly;
  }

  OmnidirectionalCalibrationParams() : rN(5) {
    recalculate();
  }
  OmnidirectionalCalibrationParams(int rN) : rN(rN) {
    recalculate();
  }

  template <typename C>
  void addParameters(C &container, const std::string &prefix) {
    AddParameter(container, prefix, "FixedStretchMatrix", fixedStretch,
                 "Set stretch matrix constant");
    AddParameter(container, prefix, "IdentityStretchMatrix", identStretch,
                 "Set stretch matrix to identity");
    AddParameter(container, prefix, "DiagonalStretchMatrix", diagonalStretch,
                 "Zero-skew stretch matrix");
    AddParameter(container, prefix, "FixPrincipalPoint", fixedPrincipal,
                 "Set principal point constant");
    AddParameter(container, prefix, "SetPrincipalPointEqual", equalPrincipal,
                 "Set principal point components equal");
    AddParameter(container, prefix, "FixPolynomial", fixedPolynom,
                 "Set polynomial distortion constant");
    AddParameter(container, prefix, "FixEvenPolynomial", fixEven,
                 "Set even degrees of polynomial distortion constant");
    AddParameter(container, prefix, "FixOddPolynomial", fixOdd,
                 "Set odd degrees of polynomial distortion constant");
    AddParameter(container, prefix, "PolynomialDegree", rN,
                 "Polynomial degree for distortion model");
  }

  void recalculate() { recalculateSizes(); }
  void recalculateSizes() {
    lS = 0;
    // 2 for affine matrix + 2 for shift + rN for polynomial
    gS = 2 + 2 + rN;

    // affine matrix
    if (!fixedStretch && !identStretch) {
      lS += 1;
      if (!diagonalStretch)
        lS += 1;
    }

    // affine shift
    if (!fixedPrincipal) {
      lS++;
      if (!equalPrincipal)
        lS++;
    }

    // WTF?!
    lS += 1;
    if (!fixedPolynom && (!fixEven || !fixOdd)) {
      if (!fixEven)
        lS += (rN - 1) / 2 + (rN - 1) % 2;
      if (!fixOdd)
        lS += (rN - 1) / 2;
    }
  }
};

struct LocalParameterizationOmnidir : public ceres::LocalParameterization {
public:
  virtual ~LocalParameterizationOmnidir() {}
  virtual int GlobalSize() const { return params.gS; }
  virtual int LocalSize() const { return params.lS; }
  OmnidirectionalCalibrationParams params;

  auto static create(const OmnidirectionalCalibrationParams &_p) {
    auto parametrization = new LocalParameterizationOmnidir();
    parametrization->params = _p;
    return parametrization;
  }

  virtual bool Plus(double const *x, double const *delta,
                    double *x_plus) const {
    int nargin = 0;
    for (int i = 0; i < GlobalSize(); ++i)
      x_plus[i] = x[i];
    if (!params.fixedStretch && !params.identStretch) {
      x_plus[0] += delta[nargin++];
      if (!params.diagonalStretch) {
        x_plus[1] += delta[nargin++];
      }
    }

    if (!params.fixedPrincipal) {
      x_plus[2] += delta[nargin];
      if (params.equalPrincipal)
        x_plus[3] += delta[nargin];
      else
        x_plus[3] += delta[++nargin];
      nargin++;
    }

    if (params.rN != 0) {
      if (!params.fixedPolynom && (!params.fixEven || !params.fixOdd)) {
        for (int i = 0; i < params.rN; ++i) {
          if (!params.fixEven && (i) % 2 == 0)
            x_plus[4 + i] += delta[nargin++];
          if (!params.fixOdd && (i) % 2 == 1)
            x_plus[4 + i] += delta[nargin++];
        }
      }
    }
    return true;
  }

  virtual bool ComputeJacobian(const double *, double *j) const {

    typedef Eigen::Matrix<double, -1, -1, Eigen::RowMajor> jt;
    Eigen::Map<jt> J(j, GlobalSize(), LocalSize());
    J.setZero();
    int nargin = 0;
    if (!params.fixedStretch && !params.identStretch) {
      J(0, nargin++) = 1.0;
      if (!params.diagonalStretch) {
        J(1, nargin++) = 1.0;
      }
    }

    if (!params.fixedPrincipal) {
      J(2, nargin) = 1.0;
      if (params.equalPrincipal)
        J(3, nargin) = 1.0;
      else
        J(3, ++nargin) = 1.0;
      nargin++;
    }

    if (params.rN != 0) {
      if (!params.fixedPolynom && (!params.fixOdd || !params.fixEven)) {
        for (int i = 0; i < params.rN; ++i) {
          if (!params.fixEven && (i) % 2 == 0)
            J(4 + i, nargin++) = 1.0;
          if (!params.fixOdd && (i) % 2 == 1)
            J(4 + i, nargin++) = 1.0;
        }
      }
    }
    return true;
  }
};

#endif // OMNIDIRECTIONALPARAMETRIZATION_H
