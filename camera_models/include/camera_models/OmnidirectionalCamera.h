#ifndef ONMIDIRECTIONALCAMERA_H
#define ONMIDIRECTIONALCAMERA_H
#include "CameraModel.h"
#include "camera_models/OmnidirectionalParametrization.h"
#include <Eigen/Eigen>
#include <ceres/ceres.h>

class OmnidirectionalModel : public CameraModel {
public:
  // this function converts model from Dmitriy`s calibration model
  // to matalb one
  void convertModel(double scale, const Eigen::Vector2d &center,
                    const Eigen::VectorXd &polyCoeffs);

  Eigen::Matrix2d getStretchMatrix() const;

  Eigen::Vector2d getPrincipal() override;

  Eigen::VectorXd getPolynom() const;

  void setPrincipal(const Eigen::Vector2d &newPrincipal);
  void setStretchMatrix(const Eigen::Matrix2d &newStretch);

  std::string getType() override { return "Omnidirectional"; }

  void saveToFile(const std::string &fname) override;
  void loadFromFile(const std::string &fname) override;

  int getDegree() const;

  void print() override;

  void exportToMatlab(const std::string &fname) override;
  void exportToOpencv(const std::string &fname) override;

  OmnidirectionalCalibrationParams calibParams;

  explicit OmnidirectionalModel(int w, int h,
                                OmnidirectionalCalibrationParams params);

  OmnidirectionalModel(const Eigen::Vector2d &stretchMatrix,
                       const Eigen::Vector2d &principal,
                       const Eigen::VectorXd &polyCoeffs);

  OmnidirectionalModel(const Eigen::Matrix2d &stretchMatrix,
                       const Eigen::Vector2d &principal,
                       const Eigen::VectorXd &polyCoeffs);

  template <typename T>
  static Eigen::Matrix<T, 2, 2>
  stretchMatrixConvert(const Eigen::Matrix<T, 2, 1> &stretchMatrix) {
    Eigen::Matrix<T, 2, 2> stretch;
    stretch << stretchMatrix[0], stretchMatrix[1], T(0.), T(1.);
    return stretch;
  }

  template <typename T>
  static Eigen::Matrix<T, 2, 1>
  project(const Eigen::Matrix<T, 3, 1> &point,
          const Eigen::Matrix<T, 2, 2> &stretchMatrix,
          const Eigen::Matrix<T, 2, 1> &principal,
          const Eigen::Matrix<T, -1, 1> &distCoeffs) {

    if (point.squaredNorm() < Eigen::NumTraits<T>::epsilon())
      return principal;

    Eigen::Matrix<T, 3, 1> normalized = point.normalized();
    Eigen::Matrix<T, 2, 1> xy = normalized.template head<2>();
    if (xy.squaredNorm() < Eigen::NumTraits<T>::epsilon())
      return principal;

    T r = point.template head<2>().norm();
    normalized = point / r;
    T z = normalized[2];

    int real_degree = distCoeffs.rows() - 1;

    Eigen::Matrix<T, -1, 1> coeffs(real_degree + 2);
    coeffs[1] = -z;
    coeffs[0] = distCoeffs(0);
    for (int i = 1; i < real_degree + 1; ++i) {
      coeffs[i + 1] = distCoeffs[i];
    }

    T firstPosRoot = solve_equation(coeffs);

    Eigen::Matrix<T, 2, 1> ans = firstPosRoot * normalized.template head<2>();
    return stretchMatrix * ans + principal;
  }

  template <typename T>
  static Eigen::Matrix<T, 2, 1>
  project(const Eigen::Matrix<T, 3, 1> &point,
          const Eigen::Matrix<T, 2, 1> &stretchMatrix,
          const Eigen::Matrix<T, 2, 1> &principal,
          const Eigen::Matrix<T, -1, 1> &distCoeffs) {
    return project(point, stretchMatrixConvert(stretchMatrix), principal,
                   distCoeffs);
  }

  template <typename T>
  static Eigen::Matrix<T, 2, 1>
  project(const Eigen::Matrix<T, 3, 1> &point,
          const Eigen::Matrix<T, -1, 1> &modelParams) {
    Eigen::Matrix<T, 2, 1> stretchMatrix = modelParams.template head<2>();
    Eigen::Matrix<T, 2, 1> principal(modelParams[2], modelParams[3]);
    Eigen::Matrix<T, -1, 1> polyCoeffs =
        modelParams.tail(modelParams.rows() - 4);
    return project(point, stretchMatrix, principal, polyCoeffs);
  }

  Eigen::Vector2d project(const Eigen::Vector3d &point) override {
    return project(point, modelParams);
  }

  template <typename T>
  static Eigen::Matrix<T, 3, 1>
  projectBack(const Eigen::Matrix<T, 2, 1> &point,
              const Eigen::Matrix<T, 2, 2> &stretchMatrix,
              const Eigen::Matrix<T, 2, 1> &principal,
              const Eigen::Matrix<T, -1, 1> &coeffs) {

    Eigen::Matrix<T, 2, 1> pt = (point - principal);

    pt = stretchMatrix.inverse() * pt;

    T rho2 = pt.squaredNorm();
    T rho1 = ceres::sqrt(rho2);

    T z = coeffs(0);
    T rho_n = rho2;
    for (int i = 1; i < coeffs.rows(); ++i) {
      z += rho_n * coeffs[i];
      rho_n *= rho1;
    }

    Eigen::Matrix<T, 3, 1> res(pt[0], pt[1], z);
    return res.normalized();
  }

  template <typename T>
  static Eigen::Matrix<T, 3, 1>
  projectBack(const Eigen::Matrix<T, 2, 1> &point,
              const Eigen::Matrix<T, 2, 1> &stretchMatrix,
              const Eigen::Matrix<T, 2, 1> &principal,
              const Eigen::Matrix<T, -1, 1> &distCoeffs) {
    return projectBack(point, stretchMatrixConvert(stretchMatrix), principal,
                       distCoeffs);
  }

  template <typename T>
  static Eigen::Matrix<T, 3, 1>
  projectBack(const Eigen::Matrix<T, 2, 1> &point,
              const Eigen::Matrix<T, -1, 1> &modelParams) {
    Eigen::Matrix<T, 2, 1> stretchMatrix = modelParams.template head<2>();
    Eigen::Matrix<T, 2, 1> principal(modelParams[2], modelParams[3]);
    Eigen::Matrix<T, -1, 1> polyCoeffs =
        modelParams.tail(modelParams.rows() - 4);
    return projectBack(point, stretchMatrix, principal, polyCoeffs);
  }

  Eigen::Vector3d projectBack(const Eigen::Vector2d &point) override {
    return projectBack(point, modelParams);
  }

  ceres::LocalParameterization *
  getLocalParametrization(CameraCalibrationParams *params) override;

  ceres::LocalParameterization *
  getLocalParametrization(OmnidirectionalCalibrationParams *params);

  void setCalibParams(CameraCalibrationParams *params) override;
  ceres::LocalParameterization *createLocalParametrization() override;

  ceres::CostFunction *
  getGlobalRelativeCost(const Eigen::Vector2d &pixel,
                        const Eigen::Vector3d &pattern) override;

  ceres::CostFunction *
  getCameraConversionCost(const Eigen::Vector2d &pixel,
                          const Eigen::Vector3d &pattern) override;

  double convertFromModel(const std::shared_ptr<CameraModel> &from) override;

private:
  double convertOptimization(const std::shared_ptr<CameraModel> &from,
                             ceres::LossFunction *loss = nullptr);
};

#endif // ONMIDIRECTIONALCAMERA_H
