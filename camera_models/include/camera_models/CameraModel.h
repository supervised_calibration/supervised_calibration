#ifndef CAMERAMODEL_H
#define CAMERAMODEL_H
#include <Eigen/Eigen>
#include <ceres/ceres.h>
#include <glog/logging.h>
#include <sophus/se3.hpp>

struct CameraCalibrationParams {
  virtual void recalculateSizes() = 0;
  virtual ~CameraCalibrationParams() {}
};

class CameraModel {
public:
  Eigen::VectorXd modelParams; // Params of the model
  Sophus::SE3d cameraPosition;
  int width, height; // Width and Height of the image

  virtual Eigen::Vector2d project(const Eigen::Vector3d &point) = 0;
  virtual Eigen::Vector3d projectBack(const Eigen::Vector2d &point) = 0;

  virtual ceres::LocalParameterization *
  getLocalParametrization(CameraCalibrationParams *params) = 0;
  virtual void setCalibParams(CameraCalibrationParams *params) = 0;
  virtual ceres::LocalParameterization *createLocalParametrization() = 0;
  // Block of cost functions for relative pose estimation
  virtual ceres::CostFunction *
  getGlobalRelativeCost(const Eigen::Vector2d &pixel,
                        const Eigen::Vector3d &pattern) = 0;
  virtual ceres::CostFunction *
  getCameraConversionCost(const Eigen::Vector2d &pixel,
                          const Eigen::Vector3d &pattern) = 0;

  virtual double convertFromModel(const std::shared_ptr<CameraModel> &from) = 0;

  virtual Eigen::Vector2d getPrincipal() = 0;

  virtual void print() = 0;
  virtual std::string getType() = 0;
  virtual void exportToMatlab(const std::string &fname) = 0;
  virtual void exportToOpencv(const std::string &fname) = 0;
  virtual void saveToFile(const std::string &fname) = 0;
  virtual void loadFromFile(const std::string &fname) = 0;
  virtual ~CameraModel() {}
};

template <typename T>
T solve_equation(const Eigen::Matrix<T, -1, 1> &polyCoeffs) {
  int N = polyCoeffs.rows();

  while (N > 0 && polyCoeffs[N - 1] == T(0.0))
    N--;

  if (polyCoeffs.rows() == 1)
    return polyCoeffs(0);

  T last_coeff = polyCoeffs[N - 1];

  N -= 2;

  Eigen::Matrix<T, -1, -1> companion(N + 1, N + 1);
  companion.setZero();
  companion.block(0, N, N + 1, 1) = -polyCoeffs.head(N + 1) / last_coeff;
  companion.block(1, 0, N, N).diagonal().setOnes();

  auto lambda = (companion.eigenvalues().eval());
  T firstPosRoot = T(std::numeric_limits<double>::infinity());
  for (int i = 0; i < lambda.rows(); ++i) {
    auto l = lambda[i];
    if (ceres::abs(l.imag()) >
            Eigen::NumTraits<T>::epsilon() * ceres::abs(l.real()) ||
        l.real() < T(0.0))
      continue;
    firstPosRoot = std::min(firstPosRoot, l.real());
  }

  return firstPosRoot;
}

#endif // CAMERAMODEL_H
