#include "camera_models/PinHoleCamera.h"
#include "camera_models/PinHoleCosts.h"
#include <fstream>
#include <iomanip>
#include <thread>

PinHoleModel::PinHoleModel(int w, int h, PinHoleCalibrationParams calibParams_)
    : radialN(calibParams_.rN), tangentialN(calibParams_.tN) {
  calibParams = calibParams_;
  // 2 - focal, 2 - principal, 1 - skew ,
  int modelParamsN = 2 + 2 + 1 + radialN + tangentialN;
  modelParams = Vx(modelParamsN);
  modelParams.setZero();
  modelParams.template head<2>() = V2((w + h) / 2, (w + h) / 2);
  modelParams[2] = w / 2.0;
  modelParams[3] = h / 2.0;
  modelParams[4] = 0;
  width = w;
  height = h;
  LOG(INFO) << "PinHole camera created params " << modelParams.transpose();
  calibParams.recalculate();
}

PinHoleModel::PinHoleModel(const V2 &focal, const V2 &principal,
                           const Vx &radial, const Vx &tangential,
                           double skew) {
  radialN = radial.rows();
  tangentialN = tangential.rows();
  modelParams = Vx(2 + 2 + 1 + radialN + tangentialN);
  modelParams =
      getModelParamsVector(focal, principal, skew, radial, tangential);
  calibParams = PinHoleCalibrationParams(radialN, tangentialN);
}

void PinHoleModel::saveToFile(const std::string &fname) {
  std::ofstream file(fname);
  file << radialN << " " << tangentialN << " ";
  file << std::setprecision(16) << std::scientific;
  file << getFocal().transpose() << " ";
  file << getPrincipal().transpose() << " ";
  file << width << " " << height << " ";
  file << getSkew() << " ";
  file << getRadialPolynom().transpose() << " ";
  file << getTangentialPolynom().transpose();
}

void PinHoleModel::loadFromFile(const std::string &fname) {
  std::ifstream file(fname);
  file >> radialN >> tangentialN;
  modelParams = Eigen::VectorXd(5 + radialN + tangentialN);
  file >> modelParams[0] >> modelParams[1]; // focal
  file >> modelParams[2] >> modelParams[3]; // principal
  file >> modelParams[4];                   // skew
  for (int i = 0; i < radialN; ++i)
    file >> modelParams[5 + i];
  for (int i = 0; i < tangentialN; ++i)
    file >> modelParams[5 + radialN + i];
  calibParams = PinHoleCalibrationParams(radialN, tangentialN);
}

Eigen::Matrix3d PinHoleModel::getCalibMatrix() const {
  Eigen::Matrix3d K = Eigen::Matrix3d::Identity();
  K(0, 0) = modelParams[0];
  K(1, 1) = modelParams[1];
  K(0, 2) = modelParams[2];
  K(1, 2) = modelParams[3];
  K(0, 1) = modelParams[4];
  return K;
}

V2 PinHoleModel::getFocal() const {
  return modelParams.template head<2>().eval();
}

V2 PinHoleModel::getPrincipal() {
  V2 principal(modelParams[2], modelParams[3]);
  return principal;
}

double PinHoleModel::getSkew() const { return modelParams[4]; }

void PinHoleModel::setFocal(const V2 &focal) {
  modelParams.template head<2>() = focal;
}
void PinHoleModel::setPrincipal(const V2 &principal) {
  modelParams[2] = principal(0);
  modelParams[3] = principal(1);
}

void PinHoleModel::setSkew(const double &skew) { modelParams[4] = skew; }

Vx PinHoleModel::getRadialPolynom() const {
  Vx radial = modelParams.tail(tangentialN + radialN).head(radialN);
  return radial;
}

Vx PinHoleModel::getTangentialPolynom() const {
  Vx tangential = modelParams.tail(tangentialN);
  return tangential;
}

void PinHoleModel::print() {
  std::cout << "\nPinHole Camera Params : \n";
  std::cout << " Focal : " << getFocal().transpose() << std::endl;
  std::cout << " Principal : " << getPrincipal().transpose() << std::endl;
  std::cout << " Skew : " << getSkew() << std::endl;
  std::cout << " Radial : " << getRadialPolynom().transpose() << std::endl;
  std::cout << " Tangentail : " << getTangentialPolynom().transpose()
            << std::endl;
}

ceres::CostFunction *
PinHoleModel::getGlobalRelativeCost(const Eigen::Vector2d &pixel,
                                    const Eigen::Vector3d &pattern) {

  return pinhole_costs::GlobalRelativeCost::create(pixel, pattern, radialN,
                                                   tangentialN, width, height);
}

void PinHoleModel::setCalibParams(CameraCalibrationParams *params) {
  calibParams = *(PinHoleCalibrationParams *)params;
}

ceres::LocalParameterization *PinHoleModel::createLocalParametrization() {
  calibParams.recalculateSizes();
  return LocalParameterizationPinHole::create(calibParams);
}

ceres::LocalParameterization *
PinHoleModel::getLocalParametrization(PinHoleCalibrationParams *params) {
  params->recalculateSizes();
  return LocalParameterizationPinHole::create(*params);
}

ceres::LocalParameterization *
PinHoleModel::getLocalParametrization(CameraCalibrationParams *params) {
  params->recalculateSizes();
  return LocalParameterizationPinHole::create(
      *(PinHoleCalibrationParams *)params);
}

ceres::CostFunction *
PinHoleModel::getCameraConversionCost(const Eigen::Vector2d &pixel,
                                      const Eigen::Vector3d &pattern) {
  return pinhole_costs::ConversionToPinHoleFunctor::create(
      pixel, pattern, radialN, tangentialN);
}

// TODO make 2 methods which estimate radial and
// tangential distortion to get rid of the same code
// here and in PinHoleProblem.cpp
double
PinHoleModel::convertFromModel(const std::shared_ptr<CameraModel> &from) {
  LOG(INFO) << "starting conversion to PinHole model";
  width = from->width;
  height = from->height;
  radialN = calibParams.rN;
  tangentialN = calibParams.tN;
  modelParams = Eigen::VectorXd(5 + radialN + tangentialN);
  LOG(INFO) << "Width = " << width;
  LOG(INFO) << "Height = " << height;
  LOG(INFO) << "Coeffs size = " << modelParams.rows();

  modelParams.setZero();
  Eigen::Vector2d pp = from->getPrincipal();
  modelParams(2) = pp(0);
  modelParams(3) = pp(1);
  LOG(INFO) << "Principal point = " << pp.transpose();
  // findig focal length
  // near prinicpal point distortion
  // expected to be small
  //
  {
    Eigen::Matrix<double, 8, 2> A;
    Eigen::VectorXd B(8);
    B.setZero();
    A.setZero();
    int inargc = 0;
    for (double i = -1; i <= 1; i++)
      for (double j = -1; j <= 1; j++) {
        if (i == 0 || j == 0)
          continue;
        Eigen::Vector2d dx(i, j);
        dx.normalize();
        Eigen::Vector3d ray = from->projectBack(pp + dx);
        auto rayh = ray.hnormalized();
        B(inargc) = dx(0);
        B(inargc + 1) = dx(1);

        A(inargc, 0) = rayh(0);
        A(inargc + 1, 1) = rayh(1);

        inargc += 2;
      }

    Eigen::Vector2d ans = A.colPivHouseholderQr().solve(B);
    LOG(INFO) << "Focal ans " << ans.transpose();
    if (calibParams.equalFocal) {
      modelParams(0) = (ans(0) + ans(1)) / 2;
      modelParams(1) = modelParams(0);
    } else {
      modelParams(0) = ans(0);
      modelParams(1) = ans(1);
    }
  }
  LOG(INFO) << "Focal = " << getFocal().transpose();
  LOG(INFO) << "Skew = " << getSkew();

  /// RADIAL DISTORTION ESTIMATION
  if (radialN != 0) {
    // starting solving radial dostortion
    int radialCoeffsN = 0;
    if (!calibParams.fixOddRadial)
      radialCoeffsN += radialN / 2 + radialN % 2;
    if (!calibParams.fixEvenRadial)
      radialCoeffsN += radialN / 2;

    int ptsN = 0;
    for (double r = 10; r < (width - pp(0)); r += double(width - pp(0)) / 100)
      ptsN++;

    Eigen::Matrix<double, -1, -1> A(2 * ptsN, radialCoeffsN);
    Eigen::VectorXd B(2 * ptsN);
    int inargcount = 0;
    for (double r = 10; r < (width - pp(0)); r += double(width - pp(0)) / 100) {
      Eigen::Vector2d u(pp(0) + r, pp(1));
      Eigen::Vector3d ray = from->projectBack(u);
      Eigen::Vector2d x = ray.hnormalized();
      Eigen::Vector2d diff = x - projectBack(u).hnormalized();

      B(inargcount) = diff(0);
      B(inargcount + 1) = diff(1);
      double r_step = r;
      radialCoeffsN = 0;
      for (int k = 0; k < radialN; ++k) {

        if (!calibParams.fixEvenRadial && (1 + k) % 2 == 0) {
          A(inargcount, radialCoeffsN) = -x(0) * r_step;
          A(inargcount + 1, radialCoeffsN++) = -x(1) * r_step;
        }

        if (!calibParams.fixOddRadial && (1 + k) % 2 == 1) {
          A(inargcount, radialCoeffsN) = -x(0) * r_step;
          A(inargcount + 1, radialCoeffsN++) = -x(1) * r_step;
        }

        r_step *= r;
      }
      inargcount += 2;
    }

    Eigen::VectorXd radialCoeffs = A.colPivHouseholderQr().solve(B);
    LOG(INFO) << "Estimated radial coeffs " << radialCoeffs.transpose();
    int radCoeffs = 0;
    for (int k = 0; k < radialN; ++k) {
      modelParams[5 + k] = 0;
      if (!calibParams.fixEvenRadial && (1 + k) % 2 == 0)
        modelParams[5 + k] = radialCoeffs[radCoeffs++];
      if (!calibParams.fixOddRadial && (1 + k) % 2 == 1)
        modelParams[5 + k] = radialCoeffs[radCoeffs++];
    }
  }
  // starting solving first two coeffs from tangential
  //
  if (tangentialN != 0) {
    int ptsN = 0;
    int maxPix = std::min(width, height);
    int N = 20;
    for (int r = -maxPix / 2 + 1; r < maxPix / 2; r += maxPix / N)
      for (double phi = 0; phi < M_PI; phi += M_PI / N)
        ptsN++;

    int inargcount = 0;
    Eigen::Matrix<double, -1, 2> A(2 * ptsN, 2);
    Eigen::VectorXd B(2 * ptsN);

    for (int R = -maxPix / 2 + 1; R < maxPix / 2; R += maxPix / N)
      for (double phi = 0; phi < M_PI; phi += M_PI / N) {
        Eigen::Vector2d u(R * cos(phi), R * sin(phi));
        u = u + from->getPrincipal();

        Eigen::Vector3d ray = from->projectBack(u);
        Eigen::Vector2d x = ray.hnormalized();

        double r = x.norm();

        Eigen::Vector2d diff;
        diff = x - projectBack(u).hnormalized();

        B(inargcount) = diff(0);

        B(inargcount + 1) = diff(1);

        A(inargcount, 0) = 2 * x(0) * x(1);
        A(inargcount, 1) = r * r + 2 * x(0) * x(0);

        A(inargcount + 1, 0) = r * r + 2 * x(1) * x(1);
        A(inargcount + 1, 1) = 2 * x(0) * x(1);

        inargcount += 2;
      }

    Eigen::Vector2d tangential;
    tangential = A.colPivHouseholderQr().solve(B);
    if (!std::isfinite(tangential(0)))
      tangential(0) = 0;
    if (!std::isfinite(tangential(1)))
      tangential(1) = 0;

    LOG(INFO) << "Estimated tangential " << tangential.transpose();
    modelParams(5 + radialN) = tangential(0);
    modelParams(5 + radialN + 1) = tangential(1);
  }
  //

  calibParams.fixedPrincipal = true;
  calibParams.recalculateSizes();
  LOG(INFO) << "Starting non linear optimization";
  double error = convertOptimization(from);
  LOG(INFO) << "Error with no Loss = " << error;
  error = convertOptimization(from, new ceres::CauchyLoss(2.0));

  calibParams.fixedPrincipal = false;
  calibParams.recalculateSizes();

  LOG(INFO) << "Error with Cauchy Loss = " << error;

  return error;
}

double
PinHoleModel::convertOptimization(const std::shared_ptr<CameraModel> &from,
                                  ceres::LossFunction *loss) {

  ceres::Problem problem;
  calibParams.recalculateSizes();
  problem.AddParameterBlock(modelParams.data(), modelParams.rows(),
                            createLocalParametrization());

  int ptN = 0;
  for (int x = 10; x < width; x += width / 40)
    for (int y = 10; y < height; y += height / 40) {
      ptN++;
      Eigen::Vector2d u(x, y);
      Eigen::Vector3d ray = from->projectBack(u);
      if (!std::isfinite(ray[0]) || !std::isfinite(ray[1]) ||
          !std::isfinite(ray[2]) || ray[2] <= 0)
        continue;
      auto cost = this->getCameraConversionCost(u, ray);

      problem.AddResidualBlock(cost, loss, modelParams.data());
    }
  ceres::Solver::Options options;
  options.linear_solver_type = ceres::DENSE_QR;
  options.minimizer_progress_to_stdout = false;
  options.num_threads = std::thread::hardware_concurrency();
  options.max_num_iterations = 1000;
  options.parameter_tolerance = 1e-16;
  options.gradient_tolerance = 1e-16;
  options.function_tolerance = 1e-16;
  ceres::Solver::Summary summary;
  ceres::Solve(options, &problem, &summary);
  LOG(INFO) << summary.FullReport();

  double init_error = sqrt(2 * summary.initial_cost / ptN);
  LOG(INFO) << " Initial RMSE = " << init_error;
  double error = sqrt(2 * summary.final_cost / ptN);
  return error;
}

void PinHoleModel::exportToOpencv(const std::string &fname) {

  Eigen::VectorXd rad(6);
  rad.setZero();
  Eigen::VectorXd tan(2);
  tan.setZero();
  Eigen::Matrix3d K;
  K.setZero();

  if (calibParams.fixOddRadial && calibParams.rN <= 6 && calibParams.rN <= 2) {
    std::cout << "Exporting without loss of accuracy. ";
    LOG(INFO) << "Exporting without loss of accuracy. ";

    K = getCalibMatrix();
    if (calibParams.rN != 0)
      rad.head(calibParams.rN) = getRadialPolynom();
    if (calibParams.tN != 0)
      tan.head(calibParams.tN) = getTangentialPolynom();

  } else {
    std::cout << "Potential Loss of accuracy. ";
    LOG(INFO) << "Potential Loss of accuracy";

    PinHoleCalibrationParams newPs = calibParams;
    newPs.rN = 6;
    newPs.tN = 2;
    newPs.fixOddRadial = true;
    newPs.recalculateSizes();
    PinHoleModel newCam(width, height, newPs);
    std::shared_ptr<PinHoleModel> cam(new PinHoleModel(*this));
    newCam.convertFromModel(cam);

    rad = newCam.getRadialPolynom();
    tan = newCam.getTangentialPolynom();
    K = newCam.getCalibMatrix();
  }

  std::ofstream outfile(fname, std::ios::out);
  outfile << "%YAML:1.0\n";

  outfile << "width: " << width << "\n";
  outfile << "height: " << height << "\n";

  outfile << "cameraMatrix: !!opencv-matrix";
  outfile << "\n   rows: 3\n   cols: 3\n   dt: d\n   data: [ ";
  outfile << K(0, 0) << ", " << K(0, 1) << ", " << K(0, 2) << ", ";
  outfile << K(1, 0) << ", " << K(1, 1) << ", " << K(1, 2) << ", ";
  outfile << K(2, 0) << ", " << K(2, 1) << ", " << K(2, 2) << " ]\n";

  outfile << "distCoeffs: !!opencv-matrix\n";
  outfile << "\n   rows: 5\n   cols: 1\n   dt: d\n   data: [ ";
  outfile << rad(1) << ", " << rad(3) << ", " << tan(0);
  outfile << ", " << tan(1) << ", " << rad(5) << " ]\n";

  outfile.close();
}
void PinHoleModel::exportToMatlab(const std::string &fname) {
  Eigen::VectorXd rad(6);
  rad.setZero();
  Eigen::VectorXd tan(2);
  tan.setZero();
  Eigen::Matrix3d K;
  K.setZero();

  if (calibParams.fixOddRadial && calibParams.rN <= 6 && calibParams.rN <= 2) {
    K = getCalibMatrix();
    if (calibParams.rN != 0)
      rad.head(calibParams.rN) = getRadialPolynom();
    if (calibParams.tN != 0)
      tan.head(calibParams.tN) = getTangentialPolynom();

  } else {
    PinHoleCalibrationParams newPs = calibParams;
    newPs.rN = 6;
    newPs.tN = 2;
    newPs.fixOddRadial = true;
    newPs.recalculateSizes();
    PinHoleModel newCam(width, height, newPs);
    std::shared_ptr<PinHoleModel> cam(new PinHoleModel(*this));
    newCam.convertFromModel(cam);

    rad = newCam.getRadialPolynom();
    tan = newCam.getTangentialPolynom();
    K = newCam.getCalibMatrix();
  }

  std::ofstream outfile(fname, std::ios::out);
  outfile << "imageSize = [" << height << " " << width << "];\n";
  outfile << "radial = [" << rad(1) << " " << rad(3) << " " << rad(5) << "];\n";
  outfile << "tangential = [" << tan(0) << " " << tan(1) << "];\n";
  outfile << "CalibMatrix = [" << K(0, 0) << " 0 0; " << K(0, 1) << " "
          << K(1, 1) << " 0; " << K(0, 2) << " " << K(1, 2) << " 1];\n";

  outfile << "cameraParams = cameraParameters('IntrinsicMatrix', CalibMatrix, "
             "'RadialDistortion', radial, 'TangentialDistortion', tangential, "
             "'ImageSize', imageSize);\n";

  outfile.close();
}
