#include <camera_models/PinHoleCamera.h>
#include <fstream>
#include <iostream>
#include <random>
#include <string>

int main() {
  Eigen::Vector2d focal(1000, 1000);
  double scew = 0.1;
  Eigen::Vector2d principal(500, 500);
  Eigen::VectorXd radial(3);
  radial(0) = 0.01;
  radial(1) = 0.001;
  radial(2) = 0.0001;
  Eigen::VectorXd tangential(3);
  tangential.setZero();
#if 1
  tangential(0) = 0.02;
  tangential(1) = 0.0045;
  tangential(2) = -0.00004;
#endif

  std::uniform_real_distribution<double> runif(0, 1000);
  std::mt19937 rng;

  std::ofstream file;
  file.open("graph.txt");

  for (int i = 0; i < 1000; ++i) {

    Eigen::Vector2d pixel(runif(rng), runif(rng));

    auto world1 = PinHoleModel::projectBack(pixel, focal, principal, scew,
                                            radial, tangential);

    double phi = atan2(world1.head(2).norm(), world1(2));

    auto pixel2 = PinHoleModel::project(world1, focal, principal, scew, radial,
                                        tangential);

    double error = (pixel - pixel2).norm();

    file << phi << " " << error << std::endl;

    PinHoleModel::projectBack(pixel, focal, principal, scew, radial,
                              tangential);
  }

  file.close();
  return 0;
}
