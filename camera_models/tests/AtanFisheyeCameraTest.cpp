#include <camera_models/AtanFisheyeCamera.h>
#include <gtest/gtest.h>
#include <iostream>
#include <random>

double getAtanNorm(Eigen::Vector3d p) {
  Eigen::Vector2d focal(1000, 1000);
  double skew = 0;
  Eigen::Vector2d principal(500, 500);
  Eigen::VectorXd coeffs(4);
  coeffs(0) = -0.042595202508066574;
  coeffs(1) = 0.031307765215775184;
  coeffs(2) = -0.04104704724832258;
  coeffs(3) = 0.015343014605793324;
  Eigen::Vector2d pixel =
      AtanFisheyeModel::project(p, focal, principal, skew, coeffs);
  Eigen::Vector3d out =
      AtanFisheyeModel::projectBack(pixel, focal, principal, skew, coeffs);
  return (out.normalized() - p.normalized()).squaredNorm();
}

const int NTEST = 1000;

TEST(AtanModelTest, FrontPointReproject) {
  std::uniform_real_distribution<double> runif(-20, 20);
  std::mt19937 rng;
  for (int i = 0; i < NTEST; ++i) {
    Eigen::Vector3d pt(runif(rng), runif(rng), runif(rng));
    if (pt.squaredNorm() == 0.0 || pt(2) == 0.0) {
      --i;
      continue;
    }
    EXPECT_GT(Eigen::NumTraits<double>::epsilon(), getAtanNorm(pt));
  }
}
