#include <camera_models/AtanFisheyeCamera.h>
#include <camera_models/OmnidirectionalCamera.h>
#include <camera_models/PinHoleCamera.h>
#include <gtest/gtest.h>
#include <memory>

class ConvertFromPinHole : public ::testing::Test {
protected:
  void SetUp() {
    Eigen::Vector2d focal(1704.63, 1697.73);
    double skew = 1.26981;
    Eigen::Vector2d principal(1283.33, 1040.9);
    Eigen::VectorXd radial(4);
    radial << 0.054085, -0.775604, 0.652932, -0.193552;

    Eigen::VectorXd tangential(2);
    tangential << 0.000127441, -2.74024e-05;
    ;

    model = std::shared_ptr<PinHoleModel>(
        new PinHoleModel(focal, principal, radial, tangential, skew));
    model->width = 2592;
    model->height = 1944;
  }

  void TearDown() {}
  std::shared_ptr<PinHoleModel> model;
};

class ConvertFromAtan : public ::testing::Test {
protected:
  void SetUp() {
    Eigen::Vector2d Focal(1707.07, 1699.96);
    Eigen::Vector2d Principal(1281.9, 1040.8);
    double skew = 1.30538;
    Eigen::VectorXd poly(7);
    poly << 0.0181391, -0.31105, 0.856404, -2.59853, 4.36901, -3.31504,
        0.876295;
    model = std::shared_ptr<AtanFisheyeModel>(
        new AtanFisheyeModel(Focal, Principal, poly, skew));
    model->width = 2592;
    model->height = 1944;
  }

  void TearDown() {}
  std::shared_ptr<AtanFisheyeModel> model;
};

class ConvertFromOmnidir : public ::testing::Test {
protected:
  void SetUp() {
    Eigen::Vector2d Stretch(1.00417, 0.000381908);
    Eigen::Vector2d Principal(1281.9, 1040.8);
    Eigen::VectorXd poly(5);
    poly << 1698.45, -0.000190003, -2.85989e-07, 2.79081e-10, -8.87487e-14;
    model = std::shared_ptr<OmnidirectionalModel>(
        new OmnidirectionalModel(Stretch, Principal, poly));
    model->width = 2592;
    model->height = 1944;
  }

  void TearDown() {}
  std::shared_ptr<OmnidirectionalModel> model;
};

TEST_F(ConvertFromPinHole, ToPinHole) {
  PinHoleCalibrationParams params(4, 2);
  std::shared_ptr<PinHoleModel> camera =
      std::shared_ptr<PinHoleModel>(new PinHoleModel(4000, 4000, params));

  double err = camera->convertFromModel(model);
  std::cout << "Error = " << err << std::endl;
  camera->print();
  EXPECT_GE(1, err);
}

TEST_F(ConvertFromAtan, ToPinHole) {
  PinHoleCalibrationParams params(4, 2);
  std::shared_ptr<PinHoleModel> camera =
      std::shared_ptr<PinHoleModel>(new PinHoleModel(4000, 4000, params));

  double err = camera->convertFromModel(model);
  std::cout << "Error = " << err << std::endl;
  camera->print();
  EXPECT_GE(1, err);
}

TEST_F(ConvertFromOmnidir, ToPinHole) {
  PinHoleCalibrationParams params(4, 2);
  std::shared_ptr<PinHoleModel> camera =
      std::shared_ptr<PinHoleModel>(new PinHoleModel(4000, 4000, params));

  double err = camera->convertFromModel(model);
  std::cout << "Error = " << err << std::endl;
  camera->print();
  EXPECT_GE(1, err);
}

TEST_F(ConvertFromPinHole, ToOmnidir) {

  OmnidirectionalCalibrationParams params(5);
  std::shared_ptr<OmnidirectionalModel> camera =
      std::shared_ptr<OmnidirectionalModel>(
          new OmnidirectionalModel(4000, 4000, params));
  double err = camera->convertFromModel(model);
  std::cout << "Error = " << err << std::endl;
  camera->print();
  EXPECT_GE(1, err);
}

TEST_F(ConvertFromAtan, ToOmnidir) {
  OmnidirectionalCalibrationParams params(5);
  std::shared_ptr<OmnidirectionalModel> camera =
      std::shared_ptr<OmnidirectionalModel>(
          new OmnidirectionalModel(4000, 4000, params));
  double err = camera->convertFromModel(model);
  std::cout << "Error = " << err << std::endl;
  camera->print();
  EXPECT_GE(1, err);
}

TEST_F(ConvertFromOmnidir, ToOmnidir) {
  OmnidirectionalCalibrationParams params(5);
  std::shared_ptr<OmnidirectionalModel> camera =
      std::shared_ptr<OmnidirectionalModel>(
          new OmnidirectionalModel(4000, 4000, params));
  double err = camera->convertFromModel(model);
  std::cout << "Error = " << err << std::endl;
  camera->print();
  EXPECT_GE(1, err);
}

TEST_F(ConvertFromPinHole, ToAtanFisheye) {

  AtanCalibrationParams params(5);
  std::shared_ptr<AtanFisheyeModel> camera = std::shared_ptr<AtanFisheyeModel>(
      new AtanFisheyeModel(4000, 4000, params));
  double err = camera->convertFromModel(model);
  std::cout << "Error = " << err << std::endl;
  camera->print();
  EXPECT_GE(1, err);
}

TEST_F(ConvertFromAtan, ToAtanFisheye) {
  AtanCalibrationParams params(5);
  std::shared_ptr<AtanFisheyeModel> camera = std::shared_ptr<AtanFisheyeModel>(
      new AtanFisheyeModel(4000, 4000, params));

  double err = camera->convertFromModel(model);
  std::cout << "Error = " << err << std::endl;
  camera->print();
  EXPECT_GE(1, err);
}

TEST_F(ConvertFromOmnidir, ToAtanFisheye) {
  AtanCalibrationParams params(5);
  std::shared_ptr<AtanFisheyeModel> camera = std::shared_ptr<AtanFisheyeModel>(
      new AtanFisheyeModel(4000, 4000, params));

  double err = camera->convertFromModel(model);
  std::cout << "Error = " << err << std::endl;
  camera->print();
  EXPECT_GE(1, err);
}
