#include "boostParams.h"
#include <camera_models/AtanFisheyeCamera.h>
#include <camera_models/OmnidirectionalCamera.h>
#include <camera_models/PinHoleCamera.h>
#include <flat_pattern_calibration/AtanFisheyeCalibration.h>
#include <flat_pattern_calibration/OmnidirectionalCalibration.h>
#include <flat_pattern_calibration/PinHoleCalibration.h>

#include <flat_pattern_calibration/CameraCalibration.h>
#include <flat_pattern_calibration/FlatPattern.h>
#include <fstream>
#include <glog/logging.h>
#include <string>
#include <vector>

int checkPattern(const std::shared_ptr<FlatPattern> &pt) { return 0; }

int rreadOne(std::ifstream &is, std::shared_ptr<FlatPattern> &pt) {
  std::string temp;
  int N;
  is >> N >> temp;
  LOG(INFO) << "Picture name " << temp << " point n " << N;
  for (int i = 0; i < N; ++i) {
    FlatPattern::PatternPair p;
    double x, y, trash;
    is >> x >> y;
    p.first = Eigen::Vector2d(x, y);
    is >> x >> y >> trash;
    p.second = Eigen::Vector3d(x, y, 1);
    pt->addPair(p);
  }
  if (checkPattern(pt) != 0)
    return 1;
  return 0;
}

void readPattern(FlatPatterns &pattern, const std::string &fname, int &w,
                 int &h) {
  pattern.clear();
  std::ifstream ifs;
  ifs.open(fname, std::ios_base::in);
  int N;
  ifs >> w >> h >> N;
  LOG(INFO) << "Patterns num " << N << " w " << w << " h " << h;
  for (int i = 0; i < N; ++i) {
    std::shared_ptr<FlatPattern> pt(new FlatPattern());
    int err = rreadOne(ifs, pt);
    if (err == 0)
      pattern.push_back(pt);
  }
}

std::shared_ptr<CameraCalibration<PinHoleModel>>
getPinHole(const int &w, const int &h, PinHoleCalibrationParams params,
           FlatPatterns &patterns) {

  auto cammodel = new PinHoleModel(w, h, params);
  std::shared_ptr<PinHoleModel> model(cammodel);
  return std::shared_ptr<CameraCalibration<PinHoleModel>>(
      new CameraCalibration<PinHoleModel>(model, patterns));
}

std::shared_ptr<CameraCalibration<OmnidirectionalModel>>
getOmnidir(const int &w, const int &h, OmnidirectionalCalibrationParams params,
           FlatPatterns &patterns) {
  std::shared_ptr<OmnidirectionalModel> model(
      new OmnidirectionalModel(w, h, params));
  return std::shared_ptr<CameraCalibration<OmnidirectionalModel>>(
      new CameraCalibration<OmnidirectionalModel>(model, patterns));
}

std::shared_ptr<CameraCalibration<AtanFisheyeModel>>
getAtan(const int &w, const int &h, AtanCalibrationParams params,
        FlatPatterns &patterns) {
  std::shared_ptr<AtanFisheyeModel> model(new AtanFisheyeModel(w, h, params));
  return std::shared_ptr<CameraCalibration<AtanFisheyeModel>>(
      new CameraCalibration<AtanFisheyeModel>(model, patterns));
}

int main(int argc, const char **argv) {
  google::InitGoogleLogging(argv[0]);
  //  google::LogToStderr();
  std::string dump;
  std::string cam_model, output;
  namespace po = boost::program_options;

  OmnidirectionalCalibrationParams omnidirectionalParams;
  AtanCalibrationParams atanParams;
  PinHoleCalibrationParams pinholeParams;

  try {
    po::options_description desc("Calibration"),
        omnidirectional("Omnidirectional model"), atan("Atan-fisheye model"),
        pinhole("Pinhole model");
    // clang-format off
    desc.add_options()
        ("help",                                                                "Print this help message")
        ("model", po::value<std::string>(&cam_model)->required(),  "Camera model: Atan, Omnidir, PinHole")
        ("output",po::value<std::string>(&output)->required(),                         "Output file name")
        ("dump" , po::value<std::string>(&dump),                         "Detected patterns");
    // clang-format on
    omnidirectionalParams.addParameters(omnidirectional, "Omnidirectional");
    atanParams.addParameters(atan, "AtanFisheye");
    pinholeParams.addParameters(pinhole, "Pinhole");

    desc.add(omnidirectional);
    desc.add(atan);
    desc.add(pinhole);

    po::variables_map vm;
    po::store(po::parse_command_line(argc, argv, desc), vm);
    if (vm.count("help")) {
      std::cout << desc << std::endl;
      return -1;
    }
    po::notify(vm);
  } catch (std::exception &e) {
    std::cout << "Error: " << e.what() << std::endl;
    return -2;
  }
  std::cout << dump << std::endl;
  int w = 0, h = 0;
  FlatPatterns pattern;
  LOG(INFO) << "Start Reading " << dump;
  readPattern(pattern, dump, w, h);
  LOG(INFO) << "Readed Patterns " << pattern.size();
  double error = 0;

  if (cam_model == "PinHole") {
    auto c = getPinHole(w, h, pinholeParams, pattern);
    error = c->calibrate();
    c->model->print();
    c->model->saveToFile(output);
  }

  if (cam_model == "Omnidir") {
    auto c = getOmnidir(w, h, omnidirectionalParams, pattern);
    error = c->calibrate();
    c->model->print();
    c->model->saveToFile(output);
  }

  if (cam_model == "Atan") {
    auto c = getAtan(w, h, atanParams, pattern);
    error = c->calibrate();
    c->model->print();
    c->model->saveToFile(output);
  }

  std::cout << "error = " << error << "\n\t" << dump << std::endl;
  return 0;
}
