#ifndef BOOST_PARAMS
#define BOOST_PARAMS

#include <boost/program_options.hpp>

template <typename T>
void AddParameter(boost::program_options::options_description &container,
                  const std::string &prefix, const std::string &name, T &t,
                  const std::string &description) {
  container.add_options()(
      ((prefix.size() ? prefix + "." : prefix) + name).c_str(),
      boost::program_options::value<T>(&t)->default_value(t),
      description.c_str());
}

#endif
