#!/usr/bin/python3
import sys
sys.path.insert(0, "../../build/wrappers/")

import super_calibration as sc
import numpy as np
from read_file import read_dump

if(len(sys.argv) < 2):
    print("You should pass valid path to DUMPS")
    exit(1)
# reading dump names from command line arguments
dumps = sys.argv[1:]

"""
 1. Creating an instance of relative pose calibrating class
"""

calib_sys = sc.SystemCalibration()

"""
 2. All dumps has the same pattern, so we will create only one instance of Pattern class (dont confuse with FlatPattern).
 Or instead you could create instance of FlatPattern with calib_sys.createFlatPattern (here you should pass Pattern class instance).
"""

pat = calib_sys.createPattern()

"""
 3. Here we need to create and calibrate camera models with parameters.
    We assume that each dump relates with its own camera.

 To make our live easier we will create set with all times.
 
"""

times_dict = set()
camera_pattern=[]

"""
List of cams is very important, 
If in the next 'for-loop' cameras wouldnt be appended to this list 
Their instances would be destructed.
"""
cams = []

for dump in dumps:
    fps, w, h = read_dump(dump)
    #fill our set with times
    for fp in fps:
        times_dict.add(fp.time)

    # Pinhole, Atan-fisheye, Omnidirectional are available.
    # Note that parameters which are not assotiated with camera wouldn`t affect on creating instance

    cam = sc.createCamera("Pinhole", w, h, radial_n=4, tangential_n=2, polynom_n=5)
    cams.append(cam)

    for i in range(len(fps)):
        fps[i].pattern = pat

    #optional return error (Anyway It would "cout" error from c++)
    error = sc.calibrateCamera(cam, fps, with_loss=True)
    # this is a wrapper around camera class with an aim to calibrate system 
    cam = calib_sys.createCamera(cam)

    # id is needed to differ cameras 
    # (to calculate final reprojection error of each camera including estimated poses)
    cam.id = dump
    camera_pattern.append((cam, fps))

"""
 4. We have prepared all input and now we are ready to build system.
 All we need is to make lists of pair (camera - pattern) for all moments of time.
"""

for t in times_dict:
    curr_time_list = []
    for pair in camera_pattern:
        for fp in pair[1]:
            if fp.time == t:
                curr_time_list.append((pair[0], fp))
    calib_sys.addObservation(curr_time_list)

calib_sys.setLoss()
calib_sys.calibrate()

for pair in camera_pattern:
    print(pair[0].pose)

