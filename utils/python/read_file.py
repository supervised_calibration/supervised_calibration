import sys
sys.path.insert(0, "../../build/wrappers/")

import super_calibration as sc
import numpy as np

def read_dump(fname):
    f = open(fname, 'r').readlines()
    W, H, N = np.array(f[0].split(' '), dtype=int)
    fps = []
    iter = 1
    for i in range(N):
        fp = sc.FlatPattern()
        line = f[iter].split(' ')
        n, s = int(line[0]), line[1]
        # FlatPattern::time is important if you want
        # to calibrate system poses 
        fp.time = s
        iter+=1
        while(n>=1):
            x, y, pX, pY, p =  np.array(f[iter].split(' '), dtype=np.float64)
            pixel = (x, y)
            pattern = (pX, pY, 1.0)
            fp.addPair(pixel, pattern)
            iter+=1
            n-=1
        if(fp.size()>4): fps.append(fp)
    return fps, W, H
