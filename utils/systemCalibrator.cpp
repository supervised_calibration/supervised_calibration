#include "boostParams.h"
#include <camera_models/AtanFisheyeCamera.h>
#include <camera_models/OmnidirectionalCamera.h>
#include <camera_models/PinHoleCamera.h>
#include <flat_pattern_calibration/AtanFisheyeCalibration.h>
#include <flat_pattern_calibration/CameraCalibration.h>
#include <flat_pattern_calibration/FlatPattern.h>
#include <flat_pattern_calibration/OmnidirectionalCalibration.h>
#include <flat_pattern_calibration/PinHoleCalibration.h>
#include <relative_pose_calibration/CameraSystemCalibration.h>

#include <boost/program_options.hpp>
#include <chrono>
#include <fstream>
#include <glog/logging.h>
#include <string>
#include <vector>

struct System {
  FlatPatterns pts;
  std::vector<std::string> time;
  std::shared_ptr<Camera> camera;
};

int rreadOne(std::ifstream &is, System &s) {
  std::shared_ptr<FlatPattern> pt(new FlatPattern());
  std::string temp;
  int N;
  is >> N >> temp;
  LOG(INFO) << "Picture name " << temp << " point n " << N;
  for (int i = 0; i < N; ++i) {
    FlatPattern::PatternPair p;
    double x, y, trash;
    is >> x >> y;
    p.first = Eigen::Vector2d(x, y);
    is >> x >> y >> trash;
    p.second = Eigen::Vector3d(x, y, 1);
    pt->addPair(p);
  }
  if (N > 4) {
    s.pts.push_back(pt);
    s.time.push_back(temp);
  }
  return 0;
}

OmnidirectionalCalibrationParams omnidirectionalParams;
AtanCalibrationParams atanParams;
PinHoleCalibrationParams pinholeParams;

std::shared_ptr<CameraCalibration<PinHoleModel>>
getPinHole(const int &w, const int &h, PinHoleCalibrationParams params,
           FlatPatterns &patterns) {

  auto cammodel = new PinHoleModel(w, h, params);
  std::shared_ptr<PinHoleModel> model(cammodel);
  return std::shared_ptr<CameraCalibration<PinHoleModel>>(
      new CameraCalibration<PinHoleModel>(model, patterns));
}

std::shared_ptr<CameraCalibration<OmnidirectionalModel>>
getOmnidir(const int &w, const int &h, OmnidirectionalCalibrationParams params,
           FlatPatterns &patterns) {
  std::shared_ptr<OmnidirectionalModel> model(
      new OmnidirectionalModel(w, h, params));
  model->calibParams.recalculate();
  return std::shared_ptr<CameraCalibration<OmnidirectionalModel>>(
      new CameraCalibration<OmnidirectionalModel>(model, patterns));
}

std::shared_ptr<CameraCalibration<AtanFisheyeModel>>
getAtan(const int &w, const int &h, AtanCalibrationParams params,
        FlatPatterns &patterns) {
  std::shared_ptr<AtanFisheyeModel> model(new AtanFisheyeModel(w, h, params));
  return std::shared_ptr<CameraCalibration<AtanFisheyeModel>>(
      new CameraCalibration<AtanFisheyeModel>(model, patterns));
}

void readDump(std::vector<System> &cams, const std::string &fname,
              const std::string &cam_model,
              const std::shared_ptr<SystemCalibrationProblem> &p) {
  int w, h;
  System s;
  FlatPatterns fps;
  std::ifstream ifs;
  ifs.open(fname, std::ios_base::in);
  int N;
  ifs >> w >> h >> N;
  LOG(INFO) << "Patterns num " << N << " w " << w << " h " << h;
  for (int i = 0; i < N; ++i)
    rreadOne(ifs, s);

  double err;
  std::shared_ptr<CameraModel> model;
  if (cam_model == "PinHole") {
    auto c = getPinHole(w, h, pinholeParams, s.pts);
    err = c->calibrate();
    model = c->model;
  }

  if (cam_model == "Omnidir") {
    omnidirectionalParams.recalculate();
    auto c = getOmnidir(w, h, omnidirectionalParams, s.pts);
    err = c->calibrate();
    model = c->model;
  }

  if (cam_model == "Atan") {
    auto c = getAtan(w, h, atanParams, s.pts);
    err = c->calibrate();
    model = c->model;
  }

  std::string folder = fname;
  int numToerase = std::string("dump.txt").length();
  auto start_pos = fname.find("dump.txt");
  if (start_pos != std::string::npos) {
    folder.erase(start_pos, numToerase);
  }

  std::cout << folder << "\n \tcalibration error = " << err << std::endl;
  s.camera = p->createCamera(model);

  s.camera->id = folder;
  cams.push_back(s);
}

void printPose(const Sophus::SE3d &a, const std::string &name) {
  std::cout << name + '\n'
            << a.rotationMatrix() << std::endl
            << a.translation().transpose() << std::endl;
}

int main(int argc, const char **argv) {
  google::InitGoogleLogging(argv[0]);
  // google::LogToStderr();
  std::vector<std::string> dumps;
  std::string cam_model;

  namespace po = boost::program_options;
  try {
    po::options_description desc("Camera system calibration"),
        omnidirectional("Omnidirectional model"), atan("Atan-fisheye model"),
        pinhole("Pinhole model");
    // clang-format off
    desc.add_options()
        ("help",                                                                "Print this help message")
        ("model", po::value<std::string>(&cam_model)->required(),  "Camera model: Atan, Omnidir, PinHole")
        ("dumps", po::value<std::vector<std::string>>(&dumps)->multitoken(),              "List of dumps");
    // clang-format on
    omnidirectionalParams.addParameters(omnidirectional, "Omnidirectional");
    atanParams.addParameters(atan, "AtanFisheye");
    pinholeParams.addParameters(pinhole, "Pinhole");

    desc.add(omnidirectional);
    desc.add(atan);
    desc.add(pinhole);
    po::variables_map vm;
    po::store(po::parse_command_line(argc, argv, desc), vm);
    if (vm.count("help")) {
      std::cout << desc << std::endl;
      return -1;
    }
    po::notify(vm);
  } catch (std::exception &e) {
    std::cout << "Error: " << e.what() << std::endl;
    return -2;
  }

  typedef std::chrono::high_resolution_clock timer;
  auto startTimer = timer::now();

  std::vector<System> cams;
  std::shared_ptr<SystemCalibrationProblem> p;
  p = std::shared_ptr<SystemCalibrationProblem>(new SystemCalibrationProblem());

  // fill system with camera infos and calibrate
  for (const std::string &fname : dumps) {
    readDump(cams, fname, cam_model, p);
  }

  std::set<std::string> timestamps;

  for (auto &cam : cams) {
    for (auto i : cam.time)
      timestamps.insert(i);
  }
  // starting building camera system...
  // there is only one pattern so...
  auto pat = p->createPattern();
  for (auto t : timestamps) {
    systemConnection camConn;
    for (size_t k = 0; k < cams.size(); ++k) {
      auto &cam = cams[k];
      for (size_t i = 0; i < cam.pts.size(); ++i) {
        if (cam.time[i] == t) {
          auto fp = p->createFlatPattern(pat, cam.pts[i]->Points,
                                         cam.pts[i]->patternToCam);
          fp->t = cam.time[i];
          camConn.push_back({cam.camera, fp});
        }
      }
    }
    auto sys = p->createSystem(camConn);
    for (auto k : camConn)
      p->addObservation(k.first, sys, k.second);
  }
  // p->setLoss();
  p->calibrate();

  auto stopTimer = timer::now();

  int count = 0;
  for (auto &cam : cams) {
    printPose(cam.camera->pose, "pose " + std::to_string(count++));
  }

  std::chrono::duration<double> dur = stopTimer - startTimer;

  std::cout << " \t all time : " << dur.count() << std::endl;

  Eigen::Matrix<double, -1, -1> Angles(cams.size(), cams.size());
  for (size_t i = 0; i < cams.size(); ++i)
    for (size_t j = 0; j < cams.size(); ++j) {
      Sophus::SO3d r1 = cams[i].camera->pose.so3();
      Sophus::SO3d r2 = cams[j].camera->pose.so3();
      double dist = ((r1 * r2.inverse()).log()).norm();
      Angles(i, j) = dist * 180 / M_PI;
    }
  std::cout << "ANGLES : \n" << Angles << std::endl;
  p->savePatterns();
  p->fullOptimization(new ceres::CauchyLoss(2.0));
  p->savePatterns();

  return 0;
}
