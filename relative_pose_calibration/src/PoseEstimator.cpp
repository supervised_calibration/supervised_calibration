#include "relative_pose_calibration/PoseEstimator.h"
#include <Eigen/Eigen>

std::vector<std::weak_ptr<Constraint>> Constraint::solve() {
  Sophus::SE3d camTpat = pattern->patternToCam;

  int knownC = 0;
  if (camera->known) {
    camTpat = camera->inverse() * camTpat;
    knownC = 1;
  }
  int knownP = 0;
  if (pattern->pattern->known) {
    camTpat = camTpat * pattern->pattern->inverse();
    knownP = 1;
  }

  if (knownC + knownP == 2) {
    system->setPose(camTpat);
    return system->connected;
  }
  if (knownC == 1) {
    camTpat = system->inverse() * camTpat;
    pattern->pattern->setPose(camTpat);
    return pattern->pattern->connected;
  }
  if (knownP == 1) {
    camTpat = camTpat * system->inverse();
    camera->setPose(camTpat);
    return camera->connected;
  }
  return {};
}
int Constraint::isResolved() {
  int res = 0;
  if (camera->known)
    res++;
  if (pattern->pattern->known)
    res++;
  if (system->known)
    res++;
  return res;
}

Sophus::SE3d Constraint::getPatternTocam() {
  Sophus::SE3d patternToCam;
  patternToCam = camera->pose * system->pose * pattern->pattern->pose;
  return patternToCam;
}
