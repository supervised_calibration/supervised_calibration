#ifndef RELATIVEPOSEFUNCTOR_H
#define RELATIVEPOSEFUNCTOR_H
#include <Eigen/Eigen>
#include <camera_models/CameraModel.h>
#include <ceres/ceres.h>
#include <flat_pattern_calibration/local_parametrization_se3.h>
#include <sophus/se3.hpp>

struct RelativeRotationFunctor {

  RelativeRotationFunctor(const Sophus::SO3d &camToPattern)
      : camToPattern(camToPattern) {}

  template <typename T>
  bool operator()(const T *patternToWorld_, const T *worldToTime_,
                  const T *systemToCam_, T *residuals) const {

    Eigen::Map<const Sophus::SO3<T>> patternToWorld(patternToWorld_);
    Eigen::Map<const Sophus::SO3<T>> worldToTime(worldToTime_);
    Eigen::Map<const Sophus::SO3<T>> systemToCam(systemToCam_);
    auto rot =
        camToPattern.cast<T>() * systemToCam * worldToTime * patternToWorld;
    Eigen::Map<Eigen::Matrix<T, 3, 1>> res(residuals);
    res = rot.log();
    return true;
  }
  Sophus::SO3d camToPattern;

  static auto create(const Sophus::SO3d &camToPattern) {
    return new ceres::AutoDiffCostFunction<RelativeRotationFunctor, 3, 4, 4, 4>(
        new RelativeRotationFunctor(camToPattern.inverse()));
  }
};

struct RelativeTranslationFunctor {

  RelativeTranslationFunctor(const Sophus::SE3d &camToPattern)
      : camToPattern(camToPattern) {}

  template <typename T>
  bool operator()(const T *patternToWorld_, const T *worldToTime_,
                  const T *systemToCam_, T *residuals) const {

    Eigen::Map<const Sophus::SE3<T>> patternToWorld(patternToWorld_);
    Eigen::Map<const Sophus::SE3<T>> worldToTime(worldToTime_);
    Eigen::Map<const Sophus::SE3<T>> systemToCam(systemToCam_);
    auto rot =
        camToPattern.cast<T>() * systemToCam * worldToTime * patternToWorld;
    Eigen::Map<Eigen::Matrix<T, 3, 1>> res(residuals);
    res = rot.translation();
    return true;
  }
  Sophus::SE3d camToPattern;

  static auto create(const Sophus::SE3d &camToPattern) {
    return new ceres::AutoDiffCostFunction<RelativeTranslationFunctor, 3, 7, 7,
                                           7>(
        new RelativeTranslationFunctor(camToPattern.inverse()));
  }
};
#endif // RELATIVEPOSEFUNCTOR_H
