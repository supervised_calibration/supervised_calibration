#ifndef POSITIONEDSYSTEM_H
#define POSITIONEDSYSTEM_H
#include "flat_pattern_calibration/UnitPosition.h"
#include <camera_models/CameraModel.h>
#include <flat_pattern_calibration/FlatPattern.h>
#include <memory>
#include <string>
#include <vector>

// camera position in the system
struct Camera : public UnitPosition {
  Camera(const std::shared_ptr<CameraModel> &cam) : cam(cam) {}
  Camera(const std::shared_ptr<CameraModel> &cam, const Sophus::SE3d &pose)
      : UnitPosition(pose), cam(cam) {}
  std::string id;
  std::shared_ptr<CameraModel> getCamera() { return cam; }
  std::shared_ptr<CameraModel> cam;
};

typedef std::vector<
    std::pair<std::shared_ptr<Camera>, std::shared_ptr<FlatPattern>>>
    systemConnection;

// System position in the moment of T
struct PositionedSystem : public UnitPosition {
  PositionedSystem(const systemConnection &connection)
      : connection(connection) {}
  systemConnection connection;
  std::string time;
};

#endif // POSITIONEDSYSTEM_H
