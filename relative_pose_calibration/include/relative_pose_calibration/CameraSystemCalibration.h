#ifndef CAMERASYSTEM_H
#define CAMERASYSTEM_H
#include "relative_pose_calibration/PoseEstimator.h"
#include "relative_pose_calibration/PositionedSystem.h"
#include <Eigen/Eigen>
#include <camera_models/AtanFisheyeCamera.h>
#include <camera_models/CameraModel.h>
#include <camera_models/OmnidirectionalCamera.h>
#include <camera_models/PinHoleCamera.h>
#include <flat_pattern_calibration/FlatPattern.h>
#include <flat_pattern_calibration/local_parametrization_se3.h>
#include <queue>
#include <sophus/se3.hpp>
#include <unordered_map>
#include <vector>

class SystemCalibrationProblem {
public:
  SystemCalibrationProblem() {}
  std::vector<std::shared_ptr<Constraint>> constr;
  // struct getters
  std::shared_ptr<Pattern> createPattern();
  std::shared_ptr<Pattern> createPattern(const Sophus::SE3d &pose);

  std::shared_ptr<FlatPattern>
  createFlatPattern(std::shared_ptr<Pattern> &pat,
                    const FlatPattern::PatternVector &p,
                    const Sophus::SE3d &patternToCam);

  std::shared_ptr<FlatPattern>
  createFlatPattern(std::shared_ptr<Pattern> &pat,
                    const FlatPattern::PatternVector &p);
  std::shared_ptr<FlatPattern> createFlatPattern(std::shared_ptr<Pattern> &pat);

  std::shared_ptr<PositionedSystem> createSystem(systemConnection &connection);

  std::shared_ptr<Camera> createCamera(const std::shared_ptr<CameraModel> &cam);
  std::shared_ptr<Camera> createCamera(const std::shared_ptr<CameraModel> &cam,
                                       const Sophus::SE3d &pose);

  void addObservation(const std::shared_ptr<Camera> &c,
                      const std::shared_ptr<PositionedSystem> &ps,
                      const std::shared_ptr<FlatPattern> &fp);

  void addObservation(systemConnection &c);

  // calibration methods
  void calibrate();
  void initializePoses();
  void optimizeRotation();
  void optimizeTranslation();
  void fullOptimization(ceres::LossFunction *loss = nullptr);
  void savePatterns();
  void setLoss(ceres::LossFunction *loss = new ceres::CauchyLoss(2.0)) {
    loss_ = loss;
  }

private:
  ceres::LossFunction *loss_ = nullptr;
  std::priority_queue<std::shared_ptr<Constraint>> queue;
  void initializec();
  bool fullResolved();
  std::set<std::shared_ptr<Camera>> camSet;
  std::set<std::shared_ptr<Pattern>> patSet;
  std::set<std::shared_ptr<PositionedSystem>> sysSet;

  // Helper function for setting parametrization
  // for optimizing problems
  template <typename Local, typename LF>
  void setParametrization(ceres::Problem &p, const int &N, LF &f) {
    setParametrization<Local>(p, N, f, camSet);
    setParametrization<Local>(p, N, f, patSet);
    setParametrization<Local>(p, N, f, sysSet);
  }

  template <typename Local, typename T, typename LF>
  void setParametrization(ceres::Problem &p, const int &N, LF &f,
                          std::set<std::shared_ptr<T>> &s) {
    for (auto &i : s) {
      if (!i->isFix())
        p.AddParameterBlock(f(i), N, new Local());
      else {
        p.AddParameterBlock(f(i), N);
        p.SetParameterBlockConstant(f(i));
      }
    }
  }
};

#endif // CAMERASYSTEM_H
