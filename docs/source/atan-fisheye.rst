
.. highlight:: c++

.. default-domain:: cpp

.. _chapter-atan-fisheye_model:


Atan-fisheye model
==================


Atan-fisheye model is based on `opencv fisheye model`_, but atan2 is used in stead of atan, so this model could project point, which are behind the camera.

.. _opencv fisheye model: https://docs.opencv.org/trunk/db/d58/group__calib3d__fisheye.html

This model is described by *Calibration matrix* and polynomial.

Сalibration matrix
------------------

Сalibration matrix K looks like 

.. math::

  K = \begin{pmatrix} f_x & \alpha & p_x \\  0 &f_y  &p_y \\  0 & 0  & 1  \end{pmatrix}, 

where :math:`(f_x, f_y)` - focal length, :math:`(p_x, p_y)` - principal point, :math:`\alpha` - skew.

Polynomial
----------

.. math::

  f(\theta) = 1 + a_1 \theta + a_2 \theta^2 + ... , 


where :math:`\theta` - angle of projecting ray.

Proejction
----------

Projection from ray to pixel point on image:

.. math::

  \begin{equation}
  r = \sqrt{X^2 + Y^2} \\
  \theta_0 = atan_2(r, Z) \\
  r_0 = f(\theta_0) \\
  \begin{pmatrix} u'  \\ v' \end{pmatrix} = 
  \frac{r_0}{r} \begin{pmatrix} X \\ Y \end{pmatrix} \\
  \begin{pmatrix} u \\ v \\ 1 \end{pmatrix} = K \begin{pmatrix} u' \\ v' \\ 1 \end{pmatrix}
  \end{equation},

where :math:`\begin{pmatrix} X \\ Y \\ Z  \end{pmatrix}` - ray, :math:`\begin{pmatrix} u \\ v \end{pmatrix}` - pixel coordinate.

  
Projection from pixel to ray is a bit more complicated.
 
.. math::

  \begin{equation} 
  \begin{pmatrix} u' \\ v' \\ 1 \end{pmatrix} = K^{-1} 
  \begin{pmatrix} u \\ v \\ 1 \end{pmatrix} \\
  r = \sqrt{{u'}^2 + {v'}^2} \\
  f'(\theta) = 1 - r + a_1\theta + a_2\theta^2 + ... \\
  \theta_0 - smallest\: real\: positive\: root\: of\: \rho'(r) \\
  \begin{pmatrix} X \\ Y \\ Z \end{pmatrix} = 
  \begin{pmatrix} \frac{sin(\theta_0)}{r} u' \\ \frac{sin(\theta_0)}{r} v' \\ cos(\theta_0) \end{pmatrix}
  \end{equation}

where :math:`\begin{pmatrix} X \\ Y \\ Z  \end{pmatrix}` - ray, :math:`\begin{pmatrix} u \\ v \end{pmatrix}` - pixel coordinate.

C++ Api
-------

Include `camera_model/AtanFisheyeCamera.h` to use this model.

:class:`AtanFisheyeModel`
_________________________

This model is implemented in :class:`AtanFisheyeModel` class. :class:`AtanFisheyeModel` could be constructed from all above parameters, stored in :class:`Eigen::Vector`.

.. code-block:: c++

  Eigen::Vector2d focal(1200, 1400);
  Eigen::Vector2d principalPoint(500, 500);
  double skew = 0.0;
  
  Eigen::VectorXd polynomial(5);
  polynomial<<1, 2, 3, 4, 5;
  
  AtanFisheyeModel atan(focal, principalPoint, polynomial, skew);
  atan.width = 120000;
  // Project 3d point to the image plane
  atan.project(Eigen::Vector3d(4, 5, 10));
  
  // Project image point to the ray
  atan.projectBack(Eigen::Vector2d(120,  250));

:class:`AtanFisheyeModel` methods
__________________________________

.. function:: Eigen::Vector2d AtanFisheyeModel::project(const Eigen::Vector3d &point)

projects 3d ray to image plane

.. function:: Eigen::Vector3d AtanFisheyeModel::projectBack(const Eigen::Vector2d &point) 

projects pixel point to 3d ray

.. function:: Eigen::Vector2d AtanFisheyeModel::getFocal()

.. function:: Eigen::Vector2d AtanFisheyeModel::getPrincipal()

.. function:: double AtanFisheyeModel::getSkew()

.. function:: Eigen::VectorXd AtanFisheyeModel::getPolynom()

.. function:: Eigen::Matrix2d PinHoleModel::getCalibMatrix()

.. function:: void AtanFisheyeModel::saveToFile(const std::string &fname)

saves parameters to file, which could be use in `AtanFisheyeModel::loadFromFile()` to load params to another pinhole model.

.. function:: void AtanFisheyeModel::loadFromFile(const std::strng &fname)

load model parameters from file generated in `AtanFisheyeModel::saveToFile()`

.. function:: void AtanFisheyeModel::prnt()

couts model parameters

.. function:: void AtanFisheyeModel::exportToOpencv(const std::string &fname)

generate YML file with current model parameters, which could be loaded in opencv

.. NOTE ::

  Due to limitations of Atab-fisheye model parameters size in OpenCV, exported model could lose accuracy in terms of reprojection error. 




:class:`AtanCalibrationParams`
______________________________

Antoher way to construct an object of :class:`AtanFisheyeModel` is to use :class:`AtanCalibrationParams`. This approach is used for constructing objects for calibration.
AtanFisheyeModel also could be constructed by width, height and AtanCalibrationParams calss instance.

.. function:: AtanCalibrationParams::AtanCalibrationParams(int polynomN)


.. code-block:: c++

  // Polynom has a degree of 5
  AtanCalibrationParams params(5);
  params.zeroSkew = true; // kew would be 0 after calibration
  AtanFisheyeModel atan(my_width, my_height, params);

Python
------

Python usage is almost the same as C++. 

Constructing from model parameters:

.. code-block:: python

  import super_calibration as sc
  focal = (100, 200)
  principal = (50, 120)
  skew = 0.5
  poly = (1,2,3,4,5,6)
  atan = sc.AtanfisheyeModel(focal, principal, poly, skew)


Constructing by :class:`AtanCalibrationParams`:

.. code-block:: python
  
  # Polynomial degree is 6
  params = sc.AtanCalibrationParams(6)
  params.equalFocal = True 
  atan = sc.AtanfisheyeModel(my_width, my_height, params)
  atan.project((2,3,4))

Python Api has some extra constructing methods

.. code-block:: python
  
  atan = sc.createCamera("Atan-fisheye", my_width, my_height, 
                            polynom_n = 5)




