
.. highlight:: c++

.. default-domain:: cpp

.. _chapter-relative_pose__calibration:

Relative Poses Calibration
============================

This task is solved by separating system in 3 connected parts:

   1. Patterns, patterns poses and patterns coordinate system
   2. Cameras, cameras poses in camera system 
   3. Camera system, camera system poses in the moment of time :math:`t_i`

So, there are 3 building blocks: Pattern, Camera, Camera system in moment :math:`t_i`.

Let Pattern :math:`i` pose in pattern camera system would be :math:`P_i`, Camera :math:`i` pose in camera system would be :math:`C_i` and camera system pose in moment :math:`t_i` would be :math:`S_i`. All these poses are SE3 elements.

Now pose of pattern :math:`i` relative to the camera :math:`j` in the moment of time :math:`k` could be written like:

.. math::

   P_{ijk} = C_jS_kP_i

Camera`s relative poses estimation could be done after calibrating cameras or after estimating pattern poses for each camera.

Relative poses estimation consists of 4 parts:

   1. Initial linear poses estimation
   2. Non-linear rotation estimation
   3. Non-linear translation estimation
   4. Non-linear all system optimization


Initial linear poses estimation
--------------------------------

For all estimated :math:`P_{ijk}` in the calibration step, we could set :math:`C_0`, :math:`S_0`, :math:`P_0` to be identity SE3 element. Then using equation :math:`P_{ijk} = C_jS_kP_i` we could solve other poses by knowing 2 poses out of 3 in the right part of equation.

It could appear that one of the cameras hasn`t got any realations with another, in such situation its Pose is set to identity and calibation would be done for 2 unbound systems.

Non-linear rotation estimation
------------------------------

On this step we optimize Rotation part of estimated on previous step poses by minimizing SO3 log. It is done by minimizing such cost function:

.. math::

   \sum \left|\left| log(P_{ijk}^{-1}C_jS_kP_i) \right|\right|^2

where all poses has only rotation part.

Non-linear translation estimation
---------------------------------

Translation optimization is done lmost like rotation optimization. Cost function looks like:

.. math::

   \sum \left|\left| translation(P_{ijk}^{-1}C_jS_kP_i) \right|\right|^2 ,

where :math:`transation()` - function wich gives translation part of the SE3 element.

Non-linear all system optimization
-----------------------------------

Final step, on this step all poses and all cameras intrinsics (which are unbound) are optimizing. It is done by minimizing distance between projected pattern point to image plane and it`s mathced pixel point. 

.. math::

   \begin{equation}
   P_{ijk} = C_jS_kP_i \\
   \sum \left( pix_q - project_i(P_{ijk}, pat_q \right)^2
   \end{equation} ,

where :math:`q` - is a pixel-pattern match index, :math:`project_i` is a camera`s :math:`i` projection function, :math:`P_{ijk}` - estimated pattern - camera pose, by using result of previous 3 steps.



