.. _installation:

Installation
=============

.. _section-source:

Getting the source code
------------------------

Firstly you need to clone the `git repository
<https://bitbucket.org/supervised_calibration/supervised_calibration/>`_.

.. code-block:: bash

   git clone https://bitbucket.org/supervised_calibration/supervised_calibration/

.. _section-dependencies:

Dependencies
-------------


For compiling source code you need to install folowing libraties:

- `Eigen <http://eigen.tuxfamily.org/index.php?title=Main_Page>`_ is required.

- `ceres-solver <http://ceres-solver.org/installation.html>`_ is required to perform camera caibration.

- `Sphinx <http://www.sphinx-doc.org/en/master/usage/installation.html>`_ is optional for compiling this documentation.

-  python 3+ is required to use bindings.

other packages are included via git submodules.

- `pybind11 <https://github.com/pybind/pybind11>`_ for python binding.

- `Sophus <https://github.com/strasdat/Sophus>`_ .

- `googletests <https://github.com/google/googletest>`_.

Compiling
---------

After installing dependencies you could compile code

.. code-block:: bash

   git submodule init
   git submodule update
   mkdir build
   cd build
   
   # to build with python wrapper add flag -DBUILD_WRAPPERS=true
   # to build docs add flag -DBUILD_DOC=true  
   cmake ../ -DCMAKE_BUILD_TYPE=Release
   make 


