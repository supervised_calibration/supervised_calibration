.. highlight:: c++

.. default-domain:: cpp


.. _quick_start:


Basic usage
===========

C++ usage
---------

Camera modes
____________

:class:`CameraModel` is inherting all camera models.
Pinhole camera model is implemented in :class:`PinHoleMode`. Atan-fisheye model is implemented in :class:`AtanFisheyeModel`. Omnidirectional camera model is implemented in :class:`OmnidirectionalModel`.

To use it you should include headers from `camera_models`.

All camera models inherted such methods:

.. function:: Eigen::Vector2d project(const Eigen::Vector3d &point)

to project ray to the image plane.

.. function:: Eigen::Vector3d projectBack(const Eigen::Vector2d &point)

to map a pixel to the ray.

All camera models could be constructed by image `WIDTH`, image `HEIGHT` and structure with parameters for each model. Otherwise they could be directly construct by model specific intrinsics.

.. code-block:: c++

  // For example constructing PinHole model:
  // Radial distortion has 8 elements in Vector, tangential - 2
  PinHoleCalibrationParams params(8, 2);
  int width = 1240, heigh = 600; 
  PinHoleModel pinhole(width , height, params);

Calibration
___________

For calibrting cameras you need to construct model with needed parameters and restrictions (`equal focal length and etc`). :class:`FlatPatten` is used to store matches between pixel coordinates of pattern and its real coordinates (one observation). This class has such methods to fill it with points:

.. function:: void addPair(const Eigen::Vector2d &point, const Eigen::Vector3d &pattern)

.. function:: void addPair(const PatternPair &p)

where :class:`PatternPair` is just a typedef for ``std::pair<Eigen::Vector2d, Eigen::Vector3d>``.

After filling vector of :class:`FlatPattern` instancies with data and constructing camera model calibration could be done:

.. code-block:: c++

  // read_data() - is your method for reading your data 
  FlatPatterns fps = read_data();
  AtanFisheyeModel atan(1000,1000, AtanCalibrationParams(5));

  CameraCalibration<AtanFIsheyeModel> calib(atan, fps);

  double err = calib.calibrate();
  std::cout<<"Camera calibration error is "<<err<<std::endl;
  // print camera parameters
  atan.print();

  //export camera parameters in opencv compatible format
  atan.exportToOpencv("atan.yml");

see `utils/calibrator.cpp` for additionl example.


Relative pose estimation
________________________

For calibrating camera system you should firstly calibrate all cameras or estimate patterns poses relative to the cameras.

For calibrating relative poses you should follow next steps:

   1. Create :class:`Pattern` instances for all different calibration patterns.
   2. Create ``FlatPatterns`` and fill it with calibration pattern detector result for all cameras.
   3. Create instance of Camera model and calibrate it.
   4. For all observations in the moment :math:`t_i` create :class:`systemConnection` and use `addObservation` method to dd it to :class:`SystemCalibrationProblem` instance.
   5. call ``SystemCalibrationProblem::calibrate()``.


an example could be found in `utils/systemCalibrato.cpp`.

Python usage
-------------

Make shure that you compiled code with python wrapper.

Camera modes
____________

All models could be constructed the same as in C++, but there is another recomended way to create an instance of camera model:

.. code-block:: python

  import super_calibration as sc

  pinhole = sc.createCamera("Pinhole", my_width, my_height, 
                            radial_n = 4, tangential_n = 2
                            equal_focal = True)

  pinhole.print() 

"Atan-fisheye" for atan-fisheye mode and "Omnidirectional" for omnidirectional model.

Calibration
_____________

In python you also need to fill list of :class:`FlatPattern` with observations.

.. code-block:: python 

   fps = []

   ...

   fp = sc.FlatPattern()
   pixel = (x, y)
   pattern = (pX, pY, 1.0)
   fp.addPair(pixel, pattern)

   ...

   fps.append(fp)

After list of observations is filled camera could be calibrated:

.. code-block:: python

   error = sc.calibrateCamera(cam, fps, with_loss=True)
   print("calibration error : ", error)

An example could be found in `utils/python/single_calibration.py`

Relative pose estimation
________________________

The same as in C++, good example with comments could be found in `utils/python/system_calibration.py`.

