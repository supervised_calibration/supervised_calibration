
.. highlight:: c++

.. default-domain:: cpp

.. _chapter-pinhole_calibration:

Pinhole calibration 
===================

Pinhole model calibration is taken from "A Flexible New Technique for Camera
Calibration" article.

Calibration could be separated into 3 stages:
   
   1. Linear estimation of calibration matrix components and initial estimation of pattern poses.
   2. Linear estimation of radial distortion and part of tangential distortion polynomial
   3. Non-linear optimization of camera parameters and pattern poses. (Optional one more optimization if using of Loss function is set)

Linear estimation of calibration matix
--------------------------------------

Firstly homography is estimating for all pixel point-pattern point matches. It is done by DLT method and then by non-linear optimization on pattern plane. 
Method for finding homography is looked like:

.. math::
   
   \begin{pmatrix}
      0 && x_i[2] u_i && - x_i[1] u_i \\
     - x_i[2] u_i && 0 && x_i[0] u_i \\
      x_i[1] u_i && -x_i[0] u && 0 \\
      &&...&&
   \end{pmatrix},

for all :math:`i = [0, N]` N - number of matching pairs of pixel point - pattern point.


Then section (3.1) from Zhang`s artice is performed to estimate and decompose calibration matrix components. And maximum likehood estimation with non-linear optimization is performed.

Linear estimation of radial polynomial
--------------------------------------

Linear estimation of radial polynomial is done by solvin next system:

.. math::

   \begin{pmatrix} -xr - xr^2 - xr^3 ... \\
   -yr -yr^2 -yr^3 ... \end{pmatrix} K = 
   \begin{pmatrix} x-u \\ y-v \end{pmatrix},

where :math:`K` - vector of radial distortion coefficients, :math:`\begin{pmatrix} u \\ v \end{pmatrix}` - distorted pixel point in normalized image coordinates, :math:`\begin{pmatrix} x \\ y \end{pmatrix}`- undisotrted point in normalize image coordinates. :math:`\begin{pmatrix} x \\ y \end{pmatrix}` is taken by pattern point projected to camera system by founded in previous step pose.

This system is solving for all pairs of pixel-pattern mathcs.

Linear estimation of tangential distortion
------------------------------------------

Linear estimation of first term of tangnetial polynomial is done by solvin next system:

.. math::

    \begin{pmatrix} (2xy) + (r^2 + 2 x^2) \\
   (r^2 + 2 y^2) + (2xy) \end{pmatrix} a = 
   \begin{pmatrix} u - x \\ v - y \end{pmatrix},
  
where :math:`a` - vector of first two tangential distortion coefficients, :math:`\begin{pmatrix} u \\ v \end{pmatrix}` - distorted pixel point in normalized image coordinates with radial undistortion, :math:`\begin{pmatrix} x \\ y \end{pmatrix}`- undisotrted point in normalize image coordinates. :math:`\begin{pmatrix} x \\ y \end{pmatrix}` is taken by pattern point projected to camera system by founded in previous step pose.

This system is solving for all pairs of pixel-pattern mathcs.

Special case
-------------

There is a special case, when estiamtion of camera caibration matrix fails, becuse of :math:`\sqrt{-|a|}`. If this situation is detected, then distortion is estimated and linear estmation of camera calibration matrix is perforemd with undistorted points.

Non-linear optimization
------------------------

On this stage we minimize reproection error by all camera model parameters and pattern poses.

.. math::

   \arg\min_{E_i, M} \left( \sum_{i=0}^{N_p} \sum_{k=0}^{N_i} \left( p_k - g(E_i, P_k, M) \right)^2 \right),

where :math:`N_p` - number of pattern pictures, :math:`E_i` - i`s pattern pose, :math:`p_i` - pixel coordinate, :math:`P_i` - pattern coordinate, :math:`M` - camera model parametes, :math:`g` - projection function from rays to image plane.


C++ Api
-------

Include `flat_pattern_caibration/PinHoleCalibration.h`.

:class:`PinHoleCalibration`
___________________________

:class:`PinHoleCalibration` is class, where Pinhole calibration is implemented.

.. function:: PinHoleCalibration::PinHoleCalibration(const std::shared_ptr<PinHoleModel> &model, FlatPatterns &patterns)

Also it supports including loss function, see `ceres-solver Loss doc`_ for addition information.

.. _ceres-solver Loss doc: http://ceres-solver.org/nnls_modeling.html#lossfunction

.. function:: void PinHoleCalibration::setLoss(ceres::LossFunction \*f = new ceres::CauchyLoss(2,0))

.. code-block:: c++

   //pinhole - is std::shared_ptr<PinHoleModel>
   // fps = FlatPatterns filled with matching pixle nd pattern points
   PinHoleCalibration calib(pinhole, fps);

   //has ceres::CauchyLoss(2.0) as default parameter
   // without this line optimization would be perform without loss function
   calib.setLoss();

   //returns RMSE of pixel reprojection
   double err = calib.calibrate();

.. function:: double PinHoleCalibration::calibrate()

perform calibration

:class:`CameraCalibration`
__________________________

Another opportunity for calibration is using :class:`CameraCalibration` class, which is templated class for calibrating camera modes. Usage could be found in `utils/calibrator.cpp` (example tool for camera caibration).
 Usage is the same:


.. code-block:: c++

   //pinhole - is std::shared_ptr<PinHoleModel>
   // fps = FlatPatterns filled with matching pixle nd pattern points
   CameraCalibration<PinHoleModel> calib(pinhole, fps);

   calib.setLoss(new myLoss(42));

   //returns RMSE of pixel reprojection
   double err = calib.calibrate();

Python
------

Calibration in Python is performed by using `calibrateCamera` function. Python Api doesn`t have option to choose Loss function, it only could be set to :class:`ceres::CauchyLoss` or none.

.. code-block:: python

   pinhole = sc.createCamera("Pinhole", my_width, my_height, 
                            radial_n = 4, tangential_n = 2
                            equal_focal = True)

   # True for using loss
   err = calibrateCamera(pinhole, fps, True)
   print("RMSE = ", err)








