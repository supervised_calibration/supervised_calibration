
.. highlight:: c++

.. default-domain:: cpp

.. _chapter-poses_estimation_usage:

C++ Api
=======

Main class where all poses calibration is implemented is :class:`SystemCalibrationProblem`. This class has some construction members to help constructing system for calibration.

:class:`Pattern`
________________

.. function:: std::shared_ptr<Pattern> SystemCalibrationProblem::createPattern()

.. function:: std::shared_ptr<Pattern> SystemCalibrationProblem::createPattern(const Sophus::SE3d \&pose)

These functions creates an instance for :class:`Pattern`, which is used to describe that cameras see the same pattern. Second function creates instance with known position in the Patterns coordinate system.

:class:`FlatPattern`
____________________

.. function:: std::shared_ptr<FlatPattern> SystemCalibrationProblem::createFlatPattern(std::shared_ptr<Pattern> &pat, const FlatPattern::PatternVector &p)

.. function:: std::shared_ptr<FlatPattern> SystemCalibrationProblem::createFlatPattern(std::shared_ptr<Pattern> \&pat)

These functions creates an instance of :class:`FlatPattern` which is used in camera calibration and stores pixel-pattern points mathcs. It has :class:`Pattern` reference, which is need to know which observation is which patter which is needed in pose calibration.

:class:`Camera`
_______________

.. function:: std::shared_ptr<Camera> SystemCalibrationProblem::createCamera(const std::shared_ptr<CameraModel> &cam, const Sophus::SE3d &pose)

.. function:: std::shared_ptr<Camera> SystemCalibrationProblem::createCamera(const std::shared_ptr<CameraModel> &cam)

These functions creates an instance of :class:`Camera`, which contain pointer to CameraModel, and stores its intrinsics and position in the system.

:class:`PositionedSystem`
__________________________

.. function:: std::shared_ptr<PositionedSystem> SystemCalibrationProblem::createSystem(systemConnection &connection)

This function creates an instance of :class:`PositionedSystem`, which described camea observations in system in time moment :math:`t_i`.

:class:`systemConnection` is just `typedef` for :class:`std::vector` of `std::pair<std::shared_ptr<Camera>, std::shared_ptr<FlatPattern>>>`, which containt Cameras and patterns visibles from them in the moment :math:`t_i`.

Observations
_____________

.. function:: void SystemCalibrationProblem::addObservation(const std::shared_ptr<Camera> &c, const std::shared_ptr<PositionedSystem> &ps, const std::shared_ptr<FlatPattern> &fp)

.. function:: void SystemCalibrationProblem::addObservation(systemConnection &c)

finally these functions add an observation to :class:`SystemCalibrationProblem` instance. It could be done manually by 1, or :class:`systemConnection` would be create in the second method realization.


.. function:: void SystemCalibrationProblem::calibrate()

this method performs calibration process.

Usage
_______

   1. Create :class:`Pattern` instances for all different calibration patterns
   2. Create :class:FlatPattern` and fill it with calibration pattern detector result
   3. Create instance of Camera model and calibrate it
   4. For all observations in the moment :math:`t_i` create :class:`systemConnection` and use `addObservation` method to dd it to :class:`SystemCalibrationProblem` instance.
   5. Set Loss Function if it is needed
   6. call `calibrate` method
