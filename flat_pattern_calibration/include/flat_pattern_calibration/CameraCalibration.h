#ifndef CAMERACALIBRATION_H
#define CAMERACALIBRATION_H
#include <camera_models/CameraModel.h>
#include <flat_pattern_calibration/AtanFisheyeCalibration.h>
#include <flat_pattern_calibration/FlatPattern.h>
#include <flat_pattern_calibration/OmnidirectionalCalibration.h>
#include <flat_pattern_calibration/PinHoleCalibration.h>
#include <memory>

template <typename CameraModel> struct CameraCalibrationBase {
  CameraCalibrationBase(const std::shared_ptr<CameraModel> &model,
                        FlatPatterns &patterns)
      : patterns(patterns), model(model) {}
  virtual double calibrate() = 0;
  virtual void setLoss() = 0;
  FlatPatterns &patterns;
  std::shared_ptr<CameraModel> model;
  ceres::LossFunction *loss = nullptr;
  virtual ~CameraCalibrationBase() { delete loss; }
};

template <typename T>
struct CameraCalibration : public CameraCalibrationBase<T> {
  CameraCalibration(const ::std::shared_ptr<T> &, FlatPatterns &) {
    std::cout << "Unsupported model" << std::endl;
  }
  double calibrate() {
    std::cout << "Unsupported model" << std::endl;
    return 0;
  }
  void setLoss() { std::cout << "Unsupported model" << std::endl; }
};

template <>
struct CameraCalibration<PinHoleModel>
    : public CameraCalibrationBase<PinHoleModel> {
  explicit CameraCalibration(const ::std::shared_ptr<PinHoleModel> &model,
                             FlatPatterns &patterns)
      : CameraCalibrationBase(model, patterns) {}
  double calibrate() {
    PinHoleCalibration calibration(model, patterns);
    calibration.setLoss(loss);
    return calibration.calibrate();
  }
  void setLoss() { loss = new ceres::CauchyLoss(2.0); }
};

template <>
struct CameraCalibration<AtanFisheyeModel>
    : public CameraCalibrationBase<AtanFisheyeModel> {
  explicit CameraCalibration(const ::std::shared_ptr<AtanFisheyeModel> &model,
                             FlatPatterns &patterns)
      : CameraCalibrationBase(model, patterns) {}
  double calibrate() {
    AtanFisheyeCalibration calibration(model, patterns);
    calibration.setLoss(loss);
    return calibration.calibrate();
  }
  void setLoss() { loss = new ceres::CauchyLoss(2.0); }
};

template <>
struct CameraCalibration<OmnidirectionalModel>
    : public CameraCalibrationBase<OmnidirectionalModel> {
  explicit CameraCalibration(
      const ::std::shared_ptr<OmnidirectionalModel> &model,
      FlatPatterns &patterns)
      : CameraCalibrationBase(model, patterns) {}
  double calibrate() {
    OmnidirectionalCalibration calibration(model, patterns);
    calibration.setLoss(loss);
    return calibration.calibrate();
  }
  void setLoss() { loss = new ceres::CauchyLoss(2.0); }
};

#endif // CAMERACALIBRATION_H
