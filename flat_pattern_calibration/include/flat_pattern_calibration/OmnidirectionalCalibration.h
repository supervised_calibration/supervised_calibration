#ifndef OMNIDIRECTIONALCALIBRATION_H
#define OMNIDIRECTIONALCALIBRATION_H

#include "camera_models/OmnidirectionalParametrization.h"
#include "camera_models/OmnidirectionalCamera.h"
#include "flat_pattern_calibration/FlatPattern.h"
#include "flat_pattern_calibration/OmnidirectionalProblem.h"
#include "flat_pattern_calibration/PosesEstimation.h"

#include <Eigen/Eigen>
#include <ceres/ceres.h>
#include <glog/logging.h>
#include <memory>
#include <vector>

class OmnidirectionalCalibration {
public:
  EIGEN_MAKE_ALIGNED_OPERATOR_NEW

  explicit OmnidirectionalCalibration(
      const std::shared_ptr<OmnidirectionalModel> &model,
      FlatPatterns &patterns)
      : model(model), patterns(patterns) {
    LOG(INFO) << "Omnidirectional calibration class created";
    LOG(INFO) << "model params " << model->modelParams.transpose();
    LOG(INFO) << "Pattern n " << patterns.size();
  }

  ceres::LossFunction *loss = nullptr;
  void setLoss(ceres::LossFunction *loss_ = new ceres::CauchyLoss(2.0)) {
    loss = loss_;
  }

  ceres::Problem problem;
  std::shared_ptr<OmnidirectionalModel> model;
  FlatPatterns &patterns;
  double calibrate();

private:
  double linearEstimation();
  double globalNNOptimization(ceres::LossFunction *loss_ = nullptr);
};

#endif // OMNIDIRECTIONALCALIBRATION_H
