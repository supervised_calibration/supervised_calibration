#ifndef PINHOLECALIBRATION_H
#define PINHOLECALIBRATION_H

#include "FlatPattern.h"
#include "camera_models/PinHoleCamera.h"
#include "flat_pattern_calibration/PinHoleProblem.h"
#include "flat_pattern_calibration/PosesEstimation.h"

#include <Eigen/Eigen>
#include <ceres/ceres.h>
#include <glog/logging.h>
#include <memory>
#include <vector>
class PinHoleCalibration {
public:
  EIGEN_MAKE_ALIGNED_OPERATOR_NEW
  explicit PinHoleCalibration(const std::shared_ptr<PinHoleModel> &model,
                              FlatPatterns &patterns)
      : model(model), patterns(patterns) {
    LOG(INFO) << "PinHole calibration class created";
    LOG(INFO) << "model params " << model->modelParams.transpose();
    LOG(INFO) << "Pattern n " << patterns.size();
  }

  std::shared_ptr<PinHoleModel> model;
  FlatPatterns &patterns;
  double calibrate();
  void setLoss(ceres::LossFunction *f = new ceres::CauchyLoss(2.0)) {
    loss = f;
  };
  ceres::LossFunction *loss = nullptr;

private:
  void linearEstimation();
  double globalNNOptimization(ceres::LossFunction *loss_ = nullptr);
};
#endif // PINHOLECALIBRATION_H
