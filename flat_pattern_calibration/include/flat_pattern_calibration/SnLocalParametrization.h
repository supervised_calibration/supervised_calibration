#ifndef SNLOCALPARAMETRIZATION_H
#define SNLOCALPARAMETRIZATION_H

#include <Eigen/Eigen>
#include <ceres/ceres.h>

template <int N> struct SNPlus {
  EIGEN_MAKE_ALIGNED_OPERATOR_NEW

  template <typename T>
  bool operator()(const T *x, const T *delta_, T *x_plus_delta) const {
    Eigen::Map<const Eigen::Matrix<T, N, 1>> normal(x, N);
    Eigen::Map<const Eigen::Matrix<T, N - 1, 1>> delta(delta_, N - 1);
    Eigen::Matrix<T, N, 1> out = normal;
    int maxInd = 0;
    T maxN = out(0) > (T)0 ? out(0): -out(0);
    for (int i = 1; i < N; ++i) {
      auto curr = out(i) > (T)0 ? out(i): -out(i);
      if ( curr > maxN)
        maxInd = i, maxN = curr;
    }
    Eigen::Map<Eigen::Matrix<T, N, 1>> deltaPlus(x_plus_delta, N);
    deltaPlus = out / maxN;

    deltaPlus.head(maxInd) += delta.head(maxInd);
    deltaPlus.tail(N - 1 - maxInd) += delta.tail(N - 1 - maxInd);
    deltaPlus.normalize();
    return true;
  }
};

#endif // SNLOCALPARAMETRIZATION_H
