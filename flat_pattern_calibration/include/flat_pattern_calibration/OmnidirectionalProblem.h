#ifndef OMNIDIRECTIONALPROBLEM_H
#define OMNIDIRECTIONALPROBLEM_H

#include "camera_models/OmnidirectionalCosts.h"
#include "camera_models/OmnidirectionalParametrization.h"
#include "camera_models/OmnidirectionalCamera.h"
#include "flat_pattern_calibration/FlatPattern.h"
#include "flat_pattern_calibration/local_parametrization_se3.h"
#include <Eigen/Eigen>
#include <memory>
#include <vector>

class OmnidirectionalProblem {
public:
  struct OmnidirectionalParams {
    double scale;
    Eigen::Vector2d center;
    Eigen::VectorXd polyCoeffs;
    OmnidirectionalParams(int N, int w, int h) {
      polyCoeffs = Eigen::VectorXd(N);
      polyCoeffs.setZero();
      polyCoeffs[0] = 1;
      scale = std::min(w, h) / 2.0;
      center = Eigen::Vector2d(w, h) / 2.0 / scale;
    }
  };

  explicit OmnidirectionalProblem(OmnidirectionalParams &model,
                                  FlatPatterns &points);

  struct PartialSolution {
    EIGEN_MAKE_ALIGNED_OPERATOR_NEW
    Eigen::Matrix<double, 3, 2> R;
    Eigen::Vector2d t;

    PartialSolution(const Eigen::Matrix<double, 3, 2> &R,
                    const Eigen::Vector2d &t)
        : R(R), t(t) {}
  };

  struct Pattern {
    EIGEN_MAKE_ALIGNED_OPERATOR_NEW

    bool poseValid = false;
    Sophus::SE3d patternToCam;
    std::string tag;
    std::vector<PartialSolution, Eigen::aligned_allocator<PartialSolution>>
        partial;

    FlatPattern points;

    int size;

    void fill(const Eigen::Vector2d &center, double scale) {
      size = points.size();
      int N = size;
      U.resize(N, 1);
      V.resize(N, 1);
      X.resize(N, 1);
      Y.resize(N, 1);
      L.resize(N, 1);

      for (int i = 0; i < N; ++i) {
        U[i] = points.getPixel(i)(0) / scale - center[0];
        V[i] = points.getPixel(i)(1) / scale - center[1];
        X[i] = points.getPattern(i)(0);
        Y[i] = points.getPattern(i)(1);
        // TODO: check Z
        L[i] = -1.0;
      }

      R2 = (U.array() * U.array() + V.array() * V.array()).matrix();
      R = R2.array().sqrt().matrix();
    }
    Eigen::VectorXd U, V, X, Y, L, R2, R;
  };

  void solveParameters(OmnidirectionalParams &model);

  std::vector<Pattern> patterns;
  double computeRMSE(const std::shared_ptr<OmnidirectionalModel> &model);
  void solvePose(Pattern &p);
  void selectPartial(Pattern &p);
  double
  patternPlaneOptimization(const std::shared_ptr<OmnidirectionalModel> &model);
};

#endif // OMNIDIRECTIONALPROBLEM_H
