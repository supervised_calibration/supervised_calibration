#include "flat_pattern_calibration/PinHoleCalibration.h"
#include <thread>

double PinHoleCalibration::calibrate() {
  if (model->calibParams.isfixed()) {
    double err =
        OnlyPosesEstimation<PinHoleModel>::estimatePoses(model, patterns);
    LOG(INFO)
        << "Only estimating poses, camera is supposed to ba calibrated error = "
        << err;
    return err;
  }
  PinHoleCalibrationParams modelParams = model->calibParams;
  LOG(INFO) << "Calibration start, rN = " << modelParams.rN
            << " tN = " << modelParams.tN;
  linearEstimation();
  LOG(INFO) << "Start NonLinearGlobalOptimization";
  double error = globalNNOptimization();
  LOG(INFO) << "Calibration Error (no Loss) = " << error;
  if (loss != nullptr) {
    error = globalNNOptimization(loss);
    LOG(INFO) << "Calibration Error (with Loss) = " << error;
  }
  return error;
}

void PinHoleCalibration::linearEstimation() {

  PinHoleProblem calibration_problem(model, patterns);
  for (int i = 0; i < 10; ++i) {
    calibration_problem.linearIntrinsicsSolve();
    calibration_problem.radialDistortionSolve();
    calibration_problem.tangentailDistortionSolve();
    LOG(INFO) << "Good estimation = " << calibration_problem.focalEstimated;
    if (calibration_problem.focalEstimated)
      break;
  }
}

double PinHoleCalibration::globalNNOptimization(ceres::LossFunction *loss_) {

  ceres::Problem problem;
  int Ntotal = 0;
  // extrinsic params estimation:
  LOG(INFO) << "Pattern N = " << patterns.size();
  for (auto &patt : patterns) {

    problem.AddParameterBlock(patt->patternToCam.data(),
                              Sophus::SE3d::num_parameters,
                              new Sophus::LocalParameterizationSE3);
    int ptsN = patt->size();
    Ntotal += ptsN;
    for (int k = 0; k < ptsN; ++k) {

      auto homoCF = pinhole_costs::PinHoleCalibrationFunctor::create(
          patt->getPixel(k), patt->getPattern(k), model->radialN,
          model->tangentialN);

      problem.AddResidualBlock(homoCF, loss_, model->modelParams.data(),
                               patt->patternToCam.data());
    }
  }
  auto pinhole_parametrization = model->createLocalParametrization();

  problem.AddParameterBlock(model->modelParams.data(),
                            model->modelParams.rows(), pinhole_parametrization);

  double final_error = 0;
  for (int k = 0; k < 3; ++k) {
    ceres::Solver::Options options;
    options.linear_solver_type = ceres::SPARSE_NORMAL_CHOLESKY;
    options.minimizer_progress_to_stdout = false;
    options.num_threads = std::thread::hardware_concurrency();
    options.max_num_iterations = 1000;
    options.parameter_tolerance = 1e-16;
    options.gradient_tolerance = 1e-16;
    options.function_tolerance = 1e-16;
    ceres::Solver::Summary summary;
    ceres::Solve(options, &problem, &summary);
    LOG(INFO) << summary.FullReport();
    double initial_error = sqrt(2 * summary.initial_cost / Ntotal);

    LOG(INFO) << "Initial error " << initial_error;
    final_error = sqrt(2 * summary.final_cost / Ntotal);
    LOG(INFO) << "Final error " << final_error;
  }
  return final_error;
}
