#include "flat_pattern_calibration/HomographyComputation.h"

Eigen::Matrix3d findHomography(const Eigen::Matrix<double, -1, 3> &left,
                               const Eigen::Matrix<double, -1, 3> &right) {

  Eigen::Matrix3d H = computeHomographyDLT(left, right);
  ceres::Problem problem;
  auto local_parameterization =
      new ceres::AutoDiffLocalParameterization<SNPlus<9>, 9, 8>;
  problem.AddParameterBlock(H.data(), 9, local_parameterization);

  for (int i = 0; i < left.rows(); ++i) {
    auto cf = HomographyTinyCost::Create(left.row(i).transpose(),
                                         right.row(i).transpose());
    problem.AddResidualBlock(cf, nullptr, H.data());
  }
  LOG(INFO) << "start NonLinear Homography";
  ceres::Solver::Options options;
  options.linear_solver_type = ceres::DENSE_QR;
  options.minimizer_progress_to_stdout = false;
  options.max_num_iterations = 1000;
  options.parameter_tolerance = 1e-10;
  options.gradient_tolerance = 1e-10;
  options.function_tolerance = 1e-10;
  ceres::Solver::Summary summary;
  ceres::Solve(options, &problem, &summary);
  LOG(INFO) << summary.FullReport() << "\n";
  double initial_error = sqrt(2 * summary.initial_cost / left.rows());
  LOG(INFO) << "Initial Homography error " << initial_error;
  double final_error = sqrt(2 * summary.final_cost / left.rows());
  LOG(INFO) << "Final Homography error " << final_error;

  return H;
}
