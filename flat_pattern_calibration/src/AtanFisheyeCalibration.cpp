#include "flat_pattern_calibration/AtanFisheyeCalibration.h"
#include "flat_pattern_calibration/HomographyComputation.h"
#include "flat_pattern_calibration/local_parametrization_se3.h"

#include <sophus/se3.hpp>
#include <stdint.h>
#include <thread>

Sophus::SE3d AtanFisheyeCalibration::bestSE3(const Sophus::SE3d &s1,
                                             const Sophus::SE3d &s2,
                                             const FlatPattern &p) {
  int s1N = 0, s2N = 0;
  for (int i = 0; i < p.size(); ++i) {
    Eigen::Vector2d pt = p.getPixel(i);
    Eigen::Vector3d ray = model->projectBack(pt);
    if (ray.dot(s1 * p.getPattern(i)) > 0)
      s1N++;
    if (ray.dot(s2 * p.getPattern(i)) > 0)
      s2N++;
  }
  if (s1N > s2N)
    return s1;
  else
    return s2;
}

double AtanFisheyeCalibration::homographyPosesEstimation() {
  // Get initial Homography
  double err = 0;
  for (auto &pattern : patterns) {
    Eigen::Matrix<double, 3, -1> goodImageRays(3, pattern->size());
    Eigen::Matrix<double, 3, -1> goodPatternRays(3, pattern->size());

    for (int j = 0; j < pattern->size(); ++j) {
      Eigen::Vector2d pixelPt = pattern->getPixel(j);
      goodImageRays.col(j) = model->projectBack(pixelPt);
      goodPatternRays.col(j) = pattern->getPattern(j);
    }

    auto H =
        findHomography(goodImageRays.transpose(), goodPatternRays.transpose());

    auto Hpair = nonLinearGomography((*model), H, *pattern);

    H = Hpair.first;
    err += Hpair.second;

    Eigen::Matrix4d manSE3;
    manSE3.setZero();
    manSE3(3, 3) = 1;
    manSE3.col(0).head(3) = H.col(0).normalized();
    manSE3.col(1).head(3) = H.col(1).normalized();
    manSE3.col(2).head(3) =
        manSE3.col(0).head<3>().cross(manSE3.col(1).head<3>());
    manSE3.col(3).head(3) =
        2 * manSE3.col(2).head(3) / (H.col(0).norm() + H.col(1).norm());
    Eigen::Matrix4d manSE3_2 = -1 * manSE3;
    manSE3_2.col(2) *= -1;
    manSE3_2(3, 3) = 1;

    pattern->patternToCam = OnlyPosesEstimation<AtanFisheyeModel>::bestSE3(
        Sophus::SE3d::fitToSE3(manSE3), Sophus::SE3d::fitToSE3(manSE3_2), model,
        *pattern);
  }
  return err / patterns.size();
}

double AtanFisheyeCalibration::calibrate() {
  if (model->calibParams.isfixed()) {
    double err =
        OnlyPosesEstimation<AtanFisheyeModel>::estimatePoses(model, patterns);
    LOG(INFO)
        << "Only estimating poses, camera is supposed to ba calibrated error = "
        << err;
    return err;
  }

  homographyPosesEstimation();
  LOG(INFO) << "Start NonLinearGlobalOptimization";
  double error = globalNNOptimization();
  LOG(INFO) << "Calibration Error (no Loss) = " << error;
  if (loss != nullptr) {
    error = globalNNOptimization(loss);
    LOG(INFO) << "Calibration Error (with Loss) = " << error;
  }
  return error;
}

double
AtanFisheyeCalibration::globalNNOptimization(ceres::LossFunction *loss_) {
  ceres::Problem problem;

  int Ntotal = 0;
  // Initialize Problem with localparam, model fields and etc
  for (auto &pattern : patterns) {

    problem.AddParameterBlock(pattern->patternToCam.data(),
                              Sophus::SE3d::num_parameters,
                              new Sophus::LocalParameterizationSE3);

    Ntotal += pattern->size();
    for (int k = 0; k < pattern->size(); ++k) {

      auto homoCF = atanfisheye_costs::AtanCalibrationFunctor::create(
          pattern->getPixel(k), pattern->getPattern(k),
          model->modelParams.rows());

      problem.AddResidualBlock(homoCF, loss_, model->modelParams.data(),
                               pattern->patternToCam.data());
    }
  }

  // addidng LocalParametrization to the block
  // implementation in AtanFisheyeParametrization.h
  problem.AddParameterBlock(model->modelParams.data(),
                            model->modelParams.rows(),
                            model->createLocalParametrization());

  double final_error = 0;
  for (int ii = 0; ii < 3; ++ii) {
    ceres::Solver::Options options;
    options.linear_solver_type = ceres::SPARSE_NORMAL_CHOLESKY;
    options.minimizer_progress_to_stdout = false;
    options.num_threads = std::thread::hardware_concurrency();
    options.max_num_iterations = 1000;
    options.parameter_tolerance = 1e-16;
    options.gradient_tolerance = 1e-16;
    options.function_tolerance = 1e-16;
    ceres::Solver::Summary summary;
    ceres::Solve(options, &problem, &summary);
    LOG(INFO) << summary.FullReport();

    LOG(INFO) << "INITIAL : " << sqrt(2 * summary.initial_cost / Ntotal);

    final_error = sqrt(2 * summary.final_cost / Ntotal);
    LOG(INFO) << "FINAL : " << final_error;
  }

  return final_error;
}
