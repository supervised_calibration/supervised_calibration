#include "flat_pattern_calibration/OmnidirectionalProblem.h"

OmnidirectionalProblem::OmnidirectionalProblem(OmnidirectionalParams &model,
                                               FlatPatterns &points) {
  for (size_t i = 0; i < points.size(); ++i) {
    points[i]->patternToCam =
        Sophus::SE3d(Eigen::Matrix3d::Identity(), Eigen::Vector3d(0, 0, 0));
    patterns.push_back(Pattern());
    patterns[i].points = *points[i];
    patterns[i].fill(model.center, model.scale);
    solvePose(patterns[i]);
  }
}

void OmnidirectionalProblem::solvePose(Pattern &p) {
  /*
   *
   *    u               /     x        \
   *    v     =  lambda | R   y   +  t |
   *   f(r)             \     0        /
   *
   *
   *
   *             x
   * r1 r2 t     y  * x[u;v;f(r)]
   *             1
   *
   *  (x r1 + y r2 + t) x [u; v; f(r)] = 0
   *
   *      i      j     k
   *    x r11   xr21  xr31
   *      u      v   f(r)
   *
   *      f(r) x r21 - v x r31       + f(r) y r22 - v y r32    + t2 f(r) - t3 v
   * = 0
   *    - f(r) x r11 + u x r31       - f(r) y r12 + u y r32    - t1 f(r) + t3 u
   * = 0
   *
   *      x r11 v - x r21 u          + y r12 v - y r22 u       + t1 v - t2 u = 0
   *
   *  => r11 r12 *    t1
   *     r21 r22 *    t2
   *      *   *  *     *
   *
   * r11^2 + r21^2 + r31^2 == alpha^2
   * r12^2 + r22^2 + r32^2 == alpha^2
   * r11 r12 + r21 r22 + r31 r32 = 0
   *
   * B+r31^2-r32^2 = 0
   * B r31^2 + r31^4 - A^2=0
   * r31 r32 = -A
   *
   * => r31^2=(B+-sqrt(B^2+4A^2))/2 => r31^2 = (B+sqrt(B^2+4A^2))/2
   *
   *   up to 2 solutions, each gives rise for 2 alpha values
   *
   *
   *   we will use one with the most votes on lambda > 0.
   *   alpha matters for t dir only
   */
  // std::cout << p.tag << std::endl;
  int N = p.size;

  Eigen::MatrixXd H(N, 6);
  H.col(0) = (p.X.array() * p.V.array()).matrix();  // r11
  H.col(1) = (p.Y.array() * p.V.array()).matrix();  // r12
  H.col(2) = -(p.X.array() * p.U.array()).matrix(); // r21
  H.col(3) = -(p.Y.array() * p.U.array()).matrix(); // r22
  H.col(4) = p.V;                                   // t1
  H.col(5) = -p.U;                                  // t2

  auto svd = H.jacobiSvd(Eigen::ComputeFullV | Eigen::ComputeThinU);

  Eigen::Matrix<double, 6, 1> res = svd.matrixV().col(5);
  double r11 = res[0], r12 = res[1], r21 = res[2], r22 = res[3], t1 = res[4],
         t2 = res[5];
  Eigen::Vector2d r1(r11, r21), r2(r12, r22), t(t1, t2);

  double A = r1.dot(r2);
  double B = r1.squaredNorm() - r2.squaredNorm();
  double r31_2 = (-B + sqrt(B * B + 4.0 * A * A)) / 2.0;
  double r31_2_sqrt = std::sqrt(r31_2);

  double sign[2] = {-1.0, 1.0};

  p.partial.clear();

  for (int i = 0; i < 2; ++i) {
    Eigen::Vector3d r1_full(r1[0], r1[1], r31_2_sqrt * sign[i]);
    Eigen::Vector3d r2_full(r2[0], r2[1], -A / r1_full[2]);

    double alpha_2 = r1_full.squaredNorm();
    double alpha_2_sqrt = std::sqrt(alpha_2);

    for (int j = 0; j < 2; ++j) {
      double alpha = alpha_2_sqrt * sign[j];

      Eigen::Vector3d r1_final = r1_full / alpha, r2_final = r2_full / alpha;
      Eigen::Vector2d t_final = t / alpha;

      res[0] = r1_final[0];
      res[1] = r2_final[0];
      res[2] = r1_final[1];
      res[3] = r2_final[1];
      res[4] = t_final[0];
      res[5] = t_final[1];

      double xy_score = 0.0;
      int pos = 0;
      for (int i = 0; i < N; ++i) {
        Eigen::Vector2d uv(p.U[i], p.V[i]);
        Eigen::Vector2d xy(p.X[i], p.Y[i]);

        Eigen::Matrix2d RR;
        RR << r1_final[0], r2_final[0], r1_final[1], r2_final[1];
        Eigen::Vector2d rxyt = RR * xy + t_final;

        double lambda = rxyt.dot(uv) / rxyt.squaredNorm();
        if (lambda > 0.0)
          ++pos;
        xy_score += (uv - lambda * rxyt).squaredNorm();
      }

      if (pos == N) {
        Eigen::Matrix<double, 3, 2> R;
        R.col(0) = r1_final;
        R.col(1) = r2_final;
        p.partial.emplace_back(R, t_final);
      }
    }
  }
  if (p.partial.size() == 0) {
    LOG(INFO) << "FAILED";
    return;
  }
  LOG(INFO) << p.partial.size() << " partial solutions";

  selectPartial(p);
}

void OmnidirectionalProblem::selectPartial(Pattern &p) {
  int N = p.size;
  // TODO: check p.partial.size() == 2;
  for (auto &ps : p.partial) {
    double r11 = ps.R(0, 0), r12 = ps.R(0, 1), r21 = ps.R(1, 0),
           r22 = ps.R(1, 1), r31 = ps.R(2, 0), r32 = ps.R(2, 1);
    double t1 = ps.t[0], t2 = ps.t[1];

    Eigen::MatrixXd M(2 * N, 3);
    Eigen::VectorXd rhs(2 * N, 1);

    Eigen::VectorXd A =
        p.X * r21 + p.Y * r22 + Eigen::VectorXd::Ones(N, 1) * t2;
    Eigen::VectorXd C =
        -p.X * r11 - p.Y * r12 - Eigen::VectorXd::Ones(N, 1) * t1;

    M.block(0, 0, N, 1) = A;
    M.block(0, 1, N, 1) = (A.array() * p.R2.array()).matrix();
    M.block(0, 2, N, 1) = -p.V;

    M.block(N, 0, N, 1) = C;
    M.block(N, 1, N, 1) = (C.array() * p.R2.array()).matrix();
    M.block(N, 2, N, 1) = p.U;

    rhs.block(0, 0, N, 1) =
        (p.V.array() * p.X.array() * r31 + p.V.array() * p.Y.array() * r32)
            .matrix();
    rhs.block(N, 0, N, 1) =
        (-p.U.array() * p.X.array() * r31 - p.U.array() * p.Y.array() * r32)
            .matrix();

    Eigen::Vector3d res =
        M.jacobiSvd(Eigen::ComputeThinV | Eigen::ComputeThinU).solve(rhs);

    double t3 = res[2];
    double rho0 = res[0];
    double rho2 = res[1];

    double xy_score = 0.0;
    int pos = 0;
    Eigen::Vector3d t(t1, t2, t3);
    for (int i = 0; i < N; ++i) {
      Eigen::Vector3d uvw(p.U[i], p.V[i], rho0 + rho2 * p.R2[i]);
      Eigen::Vector2d xy(p.X[i], p.Y[i]);

      Eigen::Vector3d rxyt = ps.R * xy + t;

      double lambda = rxyt.dot(uvw) / rxyt.squaredNorm();
      if (lambda > 0.0)
        ++pos;
      xy_score += (uvw - lambda * rxyt).squaredNorm();
    }

    if (pos == N && t[2] >= 0.0) {
      p.partial[0] = ps;
    }
  }
  if (p.partial.size() == 2)
    p.partial.erase(p.partial.begin() + 1, p.partial.end());
}

double OmnidirectionalProblem::computeRMSE(
    const std::shared_ptr<OmnidirectionalModel> &model) {
  double rmse = 0.0;
  int cnt = 0;
  int failures = 0;
  for (auto &p : patterns) {
    if (!p.partial.size())
      continue;
    for (int i = 0; i < p.size; ++i) {
      Eigen::Vector2d diff =
          model->project(p.patternToCam * p.points.getPattern(i)) -
          p.points.getPixel(i);

      if (std::isfinite(diff.squaredNorm())) {
        rmse += diff.squaredNorm();
        ++cnt;
      } else {
        ++failures;
        LOG(INFO) << "FAIL";
        LOG(INFO) << p.patternToCam * p.points.getPattern(i);
        LOG(INFO) << p.points.getPixel(i);
        LOG(INFO) << model->project(p.patternToCam * p.points.getPattern(i));
      }
    }
  }
  model->print();
  LOG(INFO) << "RMSE fit: " << std::sqrt(rmse / cnt);
  LOG(INFO) << "Projection failures: " << failures;
  return std::sqrt(rmse / cnt);
}

void OmnidirectionalProblem::solveParameters(OmnidirectionalParams &model) {
  /*
   *      f(r) x r21 - v x r31       + f(r) y r22 - v y r32    + t2 f(r) - t3 v
   * = 0
   *    - f(r) x r11 + u x r31       - f(r) y r12 + u y r31    - t1 f(r) + t3 u
   * = 0
   *
   *    r11 r12 * t1
   *    r21 r22 * t2   - are known for each frame
   *    r31 r32 *  *
   *
   *    are known => we can solve for parameters of f and t3
   */

  int Np = patterns.size();
  int N = 0;
  for (auto &p : patterns)
    if (p.partial.size())
      N += p.size;

  int D = model.polyCoeffs.rows();

  int lhs_size = N * 2;
  int rhs_size = D + Np;

  Eigen::MatrixXd M(lhs_size, rhs_size);
  Eigen::VectorXd rhs(lhs_size);
  M.block(0, D, lhs_size, Np).setZero();

  int id = D;
  int nargout = 0;
  for (auto &p : patterns) {
    if (!p.partial.size())
      continue;
    auto &ps = p.partial[0];

    double r11 = ps.R(0, 0), r12 = ps.R(0, 1), r21 = ps.R(1, 0),
           r22 = ps.R(1, 1), r31 = ps.R(2, 0), r32 = ps.R(2, 1);
    double t1 = ps.t[0], t2 = ps.t[1];

    int NN = p.size;

    Eigen::VectorXd A =
        p.X * r21 + p.Y * r22 + Eigen::VectorXd::Ones(NN, 1) * t2;
    Eigen::VectorXd C =
        -p.X * r11 - p.Y * r12 - Eigen::VectorXd::Ones(NN, 1) * t1;

    M.block(nargout, 0, NN, 1) = A;
    M.block(nargout, 1, NN, 1) = (A.array() * p.R2.array()).matrix();
    M.block(nargout, id, NN, 1) = -p.V;

    M.block(nargout + NN, 0, NN, 1) = C;
    M.block(nargout + NN, 1, NN, 1) = (C.array() * p.R2.array()).matrix();
    M.block(nargout + NN, id, NN, 1) = p.U;

    Eigen::VectorXd pow_e = p.R2;
    for (int i = 2; i < D; i += 2) {
      M.block(nargout, i, NN, 1) = (A.array() * pow_e.array()).matrix();
      M.block(nargout + NN, i, NN, 1) = (C.array() * pow_e.array()).matrix();
      if (i + 1 < D) {
        Eigen::VectorXd pow_e1 = (pow_e.array() * p.R.array()).matrix();
        M.block(nargout, i + 1, NN, 1) = (A.array() * pow_e1.array()).matrix();
        M.block(nargout + NN, i + 1, NN, 1) =
            (C.array() * pow_e1.array()).matrix();
      }
      pow_e.array() *= p.R2.array();
    }

    rhs.block(nargout, 0, NN, 1) =
        (p.V.array() * p.X.array() * r31 + p.V.array() * p.Y.array() * r32)
            .matrix();
    rhs.block(nargout + NN, 0, NN, 1) =
        (-p.U.array() * p.X.array() * r31 - p.U.array() * p.Y.array() * r32)
            .matrix();

    nargout += 2 * NN;
    id++;
  }

  Eigen::VectorXd res =
      M.jacobiSvd(Eigen::ComputeThinU | Eigen::ComputeThinV).solve(rhs);

  id = D;
  for (auto &p : patterns) {
    if (!p.partial.size())
      continue;
    Eigen::Vector3d r1 = p.partial[0].R.col(0);
    Eigen::Vector3d r2 = p.partial[0].R.col(1);
    Eigen::Vector3d r3 = r1.cross(r2);

    Eigen::Vector3d t(p.partial[0].t[0], p.partial[0].t[1], res[id]);

    Eigen::Matrix3d R;
    R.col(0) = r1;
    R.col(1) = r2;
    R.col(2) = r3;

    p.patternToCam = Sophus::SE3d(R, t);
    p.poseValid = true;

    ++id;
  }
  for (int i = 0; i < D; ++i)
    model.polyCoeffs[i] = res[i];
}

double OmnidirectionalProblem::patternPlaneOptimization(
    const std::shared_ptr<OmnidirectionalModel> &model) {
  ceres::Problem p;
  int Ntotal = 0;

  for (auto &pat : patterns) {
    if (!pat.poseValid)
      continue;
    p.AddParameterBlock(pat.patternToCam.data(), 7,
                        new Sophus::LocalParameterizationSE3());
    for (int i = 0; i < pat.points.size(); ++i) {
      Eigen::Vector2d pixel = pat.points.getPixel(i);
      Eigen::Vector3d pattern = pat.points.getPattern(i);
      auto cost = omnidirectional_costs::PatternPlaneFunctor::create(
          pixel, pattern, model->modelParams.rows());
      p.AddResidualBlock(cost, nullptr, model->modelParams.data(),
                         pat.patternToCam.data());
      Ntotal++;
    }
  }

  p.AddParameterBlock(model->modelParams.data(), model->modelParams.rows(),
                      model->createLocalParametrization());

  double final_error = 0;
  for (int ii = 0; ii < 3; ++ii) {
    ceres::Solver::Options options;
    options.linear_solver_type = ceres::SPARSE_NORMAL_CHOLESKY;
    options.minimizer_progress_to_stdout = false;
    options.num_threads = 4;
    options.max_num_iterations = 1000;
    options.parameter_tolerance = 1e-16;
    options.gradient_tolerance = 1e-16;
    options.function_tolerance = 1e-16;
    ceres::Solver::Summary summary;
    ceres::Solve(options, &p, &summary);
    LOG(INFO) << summary.FullReport() << "\n";
    final_error = sqrt(2 * summary.final_cost / Ntotal);
  }

  return final_error;
}
