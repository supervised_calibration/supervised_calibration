#include "FlatPatternGenerator.h"
#include <flat_pattern_calibration/AtanFisheyeCalibration.h>
#include <gtest/gtest.h>
#include <iostream>
#include <random>

#include <sophus/se3.hpp>

const int PatternCount = 7;

class AtanFisheyeCalibrationTest : public ::testing::Test {
protected:
  void SetUp() {
    Eigen::Vector2d focal(1000, 900);
    double skew = 0;
    Eigen::Vector2d principal(450, 643);
    Eigen::VectorXd coeffs(4);
    coeffs(0) = -0.042595202508066574;
    coeffs(1) = 0.031307765215775184;
    coeffs(2) = -0.04104704724832258;
    coeffs(3) = 0.015343014605793324;
    model = new AtanFisheyeModel(focal, principal, coeffs, skew);
    std::uniform_real_distribution<double> runif(-20, 20);
    std::uniform_real_distribution<double> angle(-1, 1);
    std::mt19937 rng;

    for (int i = 0; i < PatternCount; ++i) {
      V3 T(runif(rng), runif(rng), runif(rng));
      Eigen::Matrix3d R;
      R = Sophus::SO3d::exp(V3(angle(rng), angle(rng), angle(rng))).matrix();
      try {
        auto currPattern =
            flatPatternGenerator(*model, 1000, 1000, 4, 4, R, T, 5);
        pattern.push_back(currPattern);
      } catch (...) {
        // std::cout << "Failed to project pattern to camera..." << std::endl;
        --i;
      }
    }
  }

  void TearDown() { delete model; }
  AtanFisheyeModel *model;
  FlatPatterns pattern;

public:
  EIGEN_MAKE_ALIGNED_OPERATOR_NEW
};

TEST_F(AtanFisheyeCalibrationTest, ChessPattern) {
  AtanCalibrationParams params(7);
  std::shared_ptr<AtanFisheyeModel> testModel(
      new AtanFisheyeModel(1000, 1000, params));
  AtanFisheyeCalibration calib(testModel, pattern);

  double error = calib.calibrate();

  std::cout << "old params : \n";
  model->print();
  std::cout << "\n new params : \n";
  testModel->print();
  EXPECT_GE(1, error);
}
