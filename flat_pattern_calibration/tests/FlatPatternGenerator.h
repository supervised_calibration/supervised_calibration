#include "flat_pattern_calibration/AtanFisheyeCalibration.h"
#include "flat_pattern_calibration/FlatPattern.h"
#include "flat_pattern_calibration/OmnidirectionalCalibration.h"
#include "flat_pattern_calibration/PinHoleCalibration.h"

template <typename Camera>
std::shared_ptr<FlatPattern>
flatPatternGenerator(Camera model, double h, double w, const int &nX,
                     const int &nY, const Eigen::Matrix3d &R,
                     const Eigen::Vector3d &T, const int &step) {
  std::shared_ptr<FlatPattern> pattern(new FlatPattern());
  // Generate pattern plane with step size
  std::uniform_real_distribution<double> angle(-1, 1);
  std::mt19937 rng;

  std::vector<double> norms;
  for (int x = 0; x < nX; ++x)
    for (int y = 0; y < nY; ++y) {
      V3 pt = V3((x - nX / 2) * step, (y - nY / 2) * step, 1);
      V2 patternPt = pt.head(2);
      FlatPattern::PatternPair ppair;
      ppair.second = patternPt.homogeneous();

      pt = (R * pt) - T;
      Eigen::Vector2d noise = Eigen::Vector2d(angle(rng), angle(rng));
      V2 imagePoint = model.project(pt) + noise;
      ppair.first = imagePoint;

      pattern->addPair(ppair);

      if (model.projectBack(imagePoint).normalized().dot(pt.normalized()) <
              0.999 ||
          imagePoint[0] < 0.0 || imagePoint[1] < 0.0 || imagePoint[0] > w ||
          imagePoint[1] > h) {
        throw std::runtime_error("Invisible (?) point");
      }
    }
  for (int i = 0; i < nX; ++i)
    for (int j = 0; j < nY; ++j) {
#define IDX(a, b) ((a)*nY + (b))
      V2 this_ = pattern->getPixel(IDX(i, j));
      if (i > 0) {
        V2 left = pattern->getPixel(IDX(i - 1, j));
        norms.push_back((this_ - left).norm());
      }
      if (j > 0) {
        V2 top = pattern->getPixel(IDX(i, j - 1));
        norms.push_back((this_ - top).norm());
      }
#undef IDX
    }
  std::sort(norms.begin(), norms.end());
  std::cout << "Median NN distance: " << norms[norms.size() / 2] << std::endl;
  return pattern;
}
