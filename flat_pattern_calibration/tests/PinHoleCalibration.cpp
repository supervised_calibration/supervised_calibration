#include "FlatPatternGenerator.h"
#include <flat_pattern_calibration/PinHoleCalibration.h>
#include <gtest/gtest.h>
#include <iostream>
#include <memory>
#include <random>
#include <sophus/se3.hpp>
#include <string>

const int PatternCount = 40;

class PinHoleCalibrationTest : public ::testing::Test {
protected:
  void SetUp() {
    Eigen::Vector2d focal(1000, 1000);
    double skew = 0.1;
    Eigen::Vector2d principal(500, 500);
    Eigen::VectorXd radial(3);
    radial.setZero();
    radial(0) = 0.01;
    radial(1) = 0.001;
    radial(2) = 0.0001;
    Eigen::VectorXd tangential(3);
    tangential.setZero();
    tangential(0) = 0.02;
    tangential(1) = 0.0045;
    tangential(2) = 0.0005;
    model = new PinHoleModel(focal, principal, radial, tangential, skew);

    std::uniform_real_distribution<double> runif(-20, -1);
    std::uniform_real_distribution<double> angle(-0.5, 0.5);
    std::mt19937 rng;

    for (int i = 0; i < PatternCount; ++i) {
      Eigen::Vector3d T(runif(rng), runif(rng), runif(rng));
      Eigen::Matrix3d R;
      R = Sophus::SO3d::exp(V3(angle(rng), angle(rng), angle(rng))).matrix();
      try {
        auto currPattern =
            flatPatternGenerator(*model, 4000, 3500, 6, 6, R, T, 5);
        pattern.push_back(currPattern);
      } catch (...) {
        // std::cout << "Failed to project pattern to camera..." << std::endl;
        --i;
      }
    }
  }
  void TearDown() { delete model; }
  PinHoleModel *model;
  FlatPatterns pattern;
};

TEST_F(PinHoleCalibrationTest, ChessPattern) {
  PinHoleCalibrationParams params(8, 2);
  params.fixedSkew = true;

  std::shared_ptr<PinHoleModel> testModel(new PinHoleModel(4000, 3000, params));

  PinHoleCalibration calibration(testModel, pattern);
  calibration.setLoss();
  double error = calibration.calibrate();

  std::cout << "\n new params : \n";
  testModel->print();

  EXPECT_GE(1, error);
}
