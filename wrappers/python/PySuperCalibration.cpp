#include "CameraCalibration.h"
#include "PyCameraModels.h"
#include <Eigen/Eigen>
#include <pybind11/eigen.h>
#include <pybind11/pybind11.h>
#include <pybind11/stl.h>
#include <relative_pose_calibration/CameraSystemCalibration.h>

namespace py = pybind11;

PYBIND11_DECLARE_HOLDER_TYPE(T, std::shared_ptr<T>)
using namespace pybind11::literals;

PYBIND11_MODULE(super_calibration, m) {
  google::InitGoogleLogging("PythonModule");
  // google::LogToStderr();
  // CAMERA CALIBRATION PARAMS BLOCK

  py::class_<PinHoleCalibrationParams>(m, "PinHoleCalibrationParams")
      .def(py::init<const int &, const int &>())
      .def(py::init<>())
      .def("recalculate", &PinHoleCalibrationParams::recalculate)
      .def_readwrite("fixedFocal", &PinHoleCalibrationParams::fixedFocal)
      .def_readwrite("equalFocal", &PinHoleCalibrationParams::equalFocal)
      .def_readwrite("fixedSkew", &PinHoleCalibrationParams::fixedSkew)
      .def_readwrite("zeroSkew", &PinHoleCalibrationParams::zeroSkew)
      .def_readwrite("fixedRadial", &PinHoleCalibrationParams::fixedRadial)
      .def_readwrite("fixedTangential",
                     &PinHoleCalibrationParams::fixedTangential)
      .def_readwrite("fixOddRaidal", &PinHoleCalibrationParams::fixOddRadial)
      .def_readwrite("fixEvenRadial", &PinHoleCalibrationParams::fixEvenRadial)
      .def_readwrite("fixMainTangential",
                     &PinHoleCalibrationParams::fixMainTangential)
      .def_readwrite("fixOddTangential",
                     &PinHoleCalibrationParams::fixOddTangential)
      .def_readwrite("fixEvenTangential",
                     &PinHoleCalibrationParams::fixEvenTangential)
      .def_readwrite("fixedPrincipal",
                     &PinHoleCalibrationParams::fixedPrincipal)
      .def("__repr__", [](const PinHoleCalibrationParams &p) {
            std::string s = "<PinHoleCalibrationParams with \n";
            s = s + "radialN = " + std::to_string(p.rN)+", ";
            s = s + "tangentialN = " + std::to_string(p.tN) + "\n";
            s = s + "fixefFocal = " + (p.fixedFocal?"true\n":"false\n");
            s = s + "equalFocal = " + (p.equalFocal?"true\n":"false\n");
            s = s + "fixedSkew = " + (p.fixedSkew?"true\n":"false\n");
            s = s + "zeroSkew = " + (p.zeroSkew?"true\n":"false\n");
            s = s + "fixedRadial = " + (p.fixedRadial?"true\n":"false\n");
            s = s + "fixedTangential = " + (p.fixedTangential?"true\n":"false\n");
            s = s + "fixedPrincipal = " + (p.fixedPrincipal?"true\n":"false\n");
            s = s + "fixOddRadial = " + (p.fixOddRadial?"true\n":"false\n");
            s = s + "fixEvenRadial = " + (p.fixEvenRadial?"true\n":"false\n");
            s = s + "fixMainTangential = " + (p.fixMainTangential?"true\n":"false\n");
            s = s + "fixOddTangential = " + (p.fixOddTangential?"true\n":"false\n");
            s = s + "fixEvenTangential = " + (p.fixEvenTangential?"true\n":"false\n");
           return s + ">";
          });

  py::class_<OmnidirectionalCalibrationParams>(
      m, "OmnidirectionalCalibrationParams")
      .def(py::init<const int &>())
      .def(py::init<>())
      .def("recalculate", &OmnidirectionalCalibrationParams::recalculate)
      .def_readwrite("fixedStretch",
                     &OmnidirectionalCalibrationParams::fixedStretch)
      .def_readwrite("identStretch",
                     &OmnidirectionalCalibrationParams::identStretch)
      .def_readwrite("diagonalStretch",
                     &OmnidirectionalCalibrationParams::diagonalStretch)
      .def_readwrite("fixedPrincipal",
                     &OmnidirectionalCalibrationParams::fixedPrincipal)
      .def_readwrite("fixedPolynom",
                     &OmnidirectionalCalibrationParams::fixedPolynom)
      .def_readwrite("fixEven", &OmnidirectionalCalibrationParams::fixEven)
      .def_readwrite("fixOdd", &OmnidirectionalCalibrationParams::fixOdd)
      .def("__repr__", [](const OmnidirectionalCalibrationParams &p) {
            std::string s = "<OmnidirectionalCalibrationParams with \n";
            s = s + "polynomN = " + std::to_string(p.rN)+"\n";
            s = s + "fixedStretch = " + (p.fixedStretch?"true\n":"false\n");
            s = s + "identStretch = " + (p.identStretch?"true\n":"false\n");
            s = s + "diagonalStretch = " + (p.diagonalStretch?"true\n":"false\n");
            s = s + "fixedPrincipal = " + (p.fixedPrincipal?"true\n":"false\n");
            s = s + "fixedPolynom = " + (p.fixedPolynom?"true\n":"false\n");
            s = s + "fixOdd = " + (p.fixOdd?"true\n":"false\n");
            s = s + "fixEven = " + (p.fixEven?"true\n":"false\n");
           return s + ">";
          });


  py::class_<AtanCalibrationParams>(m, "AtanCalibrationParams")
      .def(py::init<const int &>())
      .def(py::init<>())
      .def("recalculate", &AtanCalibrationParams::recalculate)
      .def_readwrite("fixedFocal", &AtanCalibrationParams::fixedFocal)
      .def_readwrite("equalFocal", &AtanCalibrationParams::equalFocal)
      .def_readwrite("fixedPrincipal", &AtanCalibrationParams::fixedPrincipal)
      .def_readwrite("fixedPolynom", &AtanCalibrationParams::fixedPolynom)
      .def_readwrite("fixEven", &AtanCalibrationParams::fixEven)
      .def_readwrite("fixOdd", &AtanCalibrationParams::fixOdd)
      .def_readwrite("fixedSkew", &AtanCalibrationParams::fixedSkew)
      .def_readwrite("zeroSkew", &AtanCalibrationParams::zeroSkew)
      .def("__repr__", [](const AtanCalibrationParams &p) {
            std::string s = "<AtanCalibrationParams with \n";
            s = s + "polynomN = " + std::to_string(p.rN)+"\n";
            s = s + "fixedFocal = " + (p.fixedFocal?"true\n":"false\n");
            s = s + "equalFocal = " + (p.equalFocal?"true\n":"false\n");
            s = s + "fixedPrincipal = " + (p.fixedPrincipal?"true\n":"false\n");
            s = s + "fixedSkew = " + (p.fixedSkew?"true\n":"false\n");
            s = s + "zeroSkew = " + (p.zeroSkew?"true\n":"false\n");
            s = s + "fixedPolynom = " + (p.fixedPolynom?"true\n":"false\n");
            s = s + "fixOdd = " + (p.fixOdd?"true\n":"false\n");
            s = s + "fixEven = " + (p.fixEven?"true\n":"false\n");
           return s + ">";
          });


  ///
  // CAMERA MODELS BLOCK
  py::class_<CameraModel, std::shared_ptr<CameraModel>> cameraModel(
      m, "CameraModel");
  cameraModel.def("projectBack", &CameraModel::projectBack)
      .def("project", &CameraModel::project)
      .def("convertFromModel", &CameraModel::convertFromModel)
      .def("print", &CameraModel::print)
      .def("getPrincipal", &CameraModel::getPrincipal)
      .def("exportToMatlab", &CameraModel::exportToMatlab)
      .def("saveToFile", &CameraModel::saveToFile)
      .def("loadFromFile", &CameraModel::loadFromFile)
      .def("exportToOpencv", &CameraModel::exportToOpencv);

  py::class_<PinHoleModel, PyPinHoleModel>(m, "PinholeModel", cameraModel)
      .def(py::init<const int &, const int &, PinHoleCalibrationParams>())
      .def(py::init<const V2 &, const V2 &, const Vx &, const Vx &,
                    const double &>())
      .def("print", &CameraModel::print)
      .def("convertFromModel", &PinHoleModel::convertFromModel)
      .def("getSkew", &PinHoleModel::getSkew)
      .def("setSkew", &PinHoleModel::setSkew)
      .def("getFocal", &PinHoleModel::getFocal)
      .def("setFocal", &PinHoleModel::setFocal)
      .def("getPrincipal", &CameraModel::getPrincipal)
      .def("setPrincipal", &PinHoleModel::setPrincipal)
      .def("getRadialPolynom", &PinHoleModel::getRadialPolynom)
      .def("getTangentialPolynom", &PinHoleModel::getTangentialPolynom)
      .def("projectBack", &CameraModel::projectBack)
      .def("project", &CameraModel::project)
      .def("exportToMatlab", &CameraModel::exportToMatlab)
      .def("exportToOpencv", &CameraModel::exportToOpencv)
      .def("loadFromFile", &CameraModel::loadFromFile)
      .def("saveToFile", &CameraModel::saveToFile)
      .def_readwrite("calib_param", &PinHoleModel::calibParams);

  py::class_<AtanFisheyeModel, PyAtanFisheyeModel>(m, "AtanModel", cameraModel)
      .def(py::init<const int &, const int &, AtanCalibrationParams>())
      .def(py::init<const V2 &, const V2 &, const Vx &, const double &>())
      .def("print", &CameraModel::print)
      .def("convertFromModel", &AtanFisheyeModel::convertFromModel)
      .def("getSkew", &AtanFisheyeModel::getSkew)
      .def("setSkew", &AtanFisheyeModel::setSkew)
      .def("getFocal", &AtanFisheyeModel::getFocal)
      .def("setFocal", &AtanFisheyeModel::setFocal)
      .def("getPrincipal", &CameraModel::getPrincipal)
      .def("setPrincipal", &AtanFisheyeModel::setPrincipal)
      .def("getPolynom", &AtanFisheyeModel::getPolynom)
      .def("projectBack", &CameraModel::projectBack)
      .def("project", &CameraModel::project)
      .def("exportToMatlab", &CameraModel::exportToMatlab)
      .def("exportToOpencv", &CameraModel::exportToOpencv)
      .def("loadFromFile", &CameraModel::loadFromFile)
      .def("saveToFile", &CameraModel::saveToFile)
      .def_readwrite("calib_param", &AtanFisheyeModel::calibParams);

  py::class_<OmnidirectionalModel, PyOmnidirectionalModel>(
      m, "OmnidirectionalModel", cameraModel)
      .def(py::init<const int &, const int &,
                    OmnidirectionalCalibrationParams>())
      .def(py::init<const Eigen::Matrix2d &, const V2 &, const Vx &>())
      .def("print", &CameraModel::print)
      .def("convertFromModel", &OmnidirectionalModel::convertFromModel)
      .def("getStretchMatrix", &OmnidirectionalModel::getStretchMatrix)
      .def("setStretchMatrix", &OmnidirectionalModel::setStretchMatrix)
      .def("getPrincipal", &CameraModel::getPrincipal)
      .def("setPrincipal", &OmnidirectionalModel::setPrincipal)
      .def("getPolynom", &OmnidirectionalModel::getPolynom)
      .def("projectBack", &CameraModel::projectBack)
      .def("project", &CameraModel::project)
      .def("exportToMatlab", &CameraModel::exportToMatlab)
      .def("loadFromFile", &CameraModel::loadFromFile)
      .def("saveToFile", &CameraModel::saveToFile)
      .def("exportToOpencv", &CameraModel::exportToOpencv)
      .def_readwrite("calib_param", &OmnidirectionalModel::calibParams);

  // CAMERA CALIBRATION
  py::class_<Sophus::SE3d>(m, "Pose")
      .def(py::init<const Eigen::Matrix3d &, const Eigen::Vector3d &>())
      .def(py::init([](const Eigen::Vector4d &a, const Eigen::Vector3d &b) {
        return std::unique_ptr<Sophus::SE3d>(
            new Sophus::SE3d(Eigen::Quaterniond(a), b));
      }))
      .def(py::init<>())
      .def("__repr__", [](const Sophus::SE3d &a) {
        std::string out = "<Pose with R : \n";
        out = out + EigentoString(a.rotationMatrix());
        out = out + "\n translation : ";
        out = out + EigentoString(a.translation().transpose());
        out = out + ">";
        return out;
      });

  py::class_<Pattern, std::shared_ptr<Pattern>>(m, "Pattern")
      .def(py::init<>())
      .def(py::init<const Sophus::SE3d &>())
      .def_readwrite("pose", &UnitPosition::pose)
      .def_readwrite("id", &Pattern::id)
      .def("__repr__", [](const Pattern &a) {
        return "<Pattern with id  '" + a.id + "'>";
      });

  py::class_<FlatPattern, std::shared_ptr<FlatPattern>>(m, "FlatPattern")
      .def(py::init<>())
      .def("addPair",
           py::overload_cast<const V2 &, const V3 &>(&FlatPattern::addPair))
      .def("addPair",
           py::overload_cast<const FlatPattern::PatternPair &>(&FlatPattern::addPair))
      .def("getPixel", &FlatPattern::getPixel)
      .def("getPattern", &FlatPattern::getPattern)
      .def_readwrite("points", &FlatPattern::Points)
      .def_readwrite("pattern", &FlatPattern::pattern)
      .def("size", &FlatPattern::size)
      .def_readwrite("time", &FlatPattern::t)
      .def("getToCamPose",
           [](const FlatPattern &fp) {
             Eigen::Matrix3d R = fp.patternToCam.rotationMatrix();
             return std::pair<Eigen::Matrix3d, Eigen::Vector3d>(
                 R, fp.patternToCam.translation());
           })
      .def("__repr__", [](const FlatPattern &fp) {
        return "<FlatPatterin of size " + std::to_string(fp.size()) + ">";
      });

  m.def("createCamera", &createCamera,
        "Function which creates camera by string name{Pinhole, Atan-fisheye, "
        "Omnidirectional}, W nad H are required",
        "model"_a, "w"_a, "h"_a, "radial_n"_a = 4, "tangential_n"_a = 2,
        "polynom_n"_a = 5, "fix_focal"_a = false, "equal_focal"_a = false,
        "fix_principal"_a = false, "fix_skew"_a = false, "zero_skew"_a = false,
        "fix_stretch"_a = false, "identity_stretch"_a = false,
        "diagonal_stretch"_a = false, "fix_polynom"_a = false,
        "fix_tangential"_a = false, "fix_radial"_a = false,
        "fix_even_radial"_a = false, "fix_odd_radial"_a = false,
        "fix_even_polynom"_a = false, "fix_odd_polynom"_a = false);

  m.def("calibrateCamera",
        [](const std::shared_ptr<CameraModel> &model, FlatPatterns &patterns,
           bool with_loss = false) {
          return calibrateCamera(model, patterns, with_loss);
        },
        "model"_a, "flat_patterns"_a, "with_loss"_a = false);

  // RELATIVE POSE CALIBRATION
  py::class_<Camera, std::shared_ptr<Camera>>(m, "Camera")
      .def(py::init<const std::shared_ptr<CameraModel> &>())
      .def(py::init<const std::shared_ptr<CameraModel> &,
                    const Sophus::SE3d &>())
      .def_readwrite("id", &Camera::id)
      .def("getCamera", &Camera::getCamera)
      .def_readwrite("pose", &UnitPosition::pose);

  py::class_<Constraint, std::shared_ptr<Constraint>>(m, "Constraint")
      .def(py::init<const std::shared_ptr<Camera> &,
                    const std::shared_ptr<FlatPattern> &,
                    std::shared_ptr<PositionedSystem> &>());

  py::class_<PositionedSystem, std::shared_ptr<PositionedSystem>>(
      m, "PositionedSystem")
      .def(py::init<const systemConnection &>())
      .def_readwrite("time", &PositionedSystem::time)
      .def_readwrite("pose", &UnitPosition::pose);

  py::class_<SystemCalibrationProblem,
             std::shared_ptr<SystemCalibrationProblem>>(m, "SystemCalibration")
      .def(py::init<>())
      .def("setLoss",
           [](const std::shared_ptr<SystemCalibrationProblem> &s) {
             s->setLoss();
             return 0;
           })
      .def("createPattern",
           py::overload_cast<>(&SystemCalibrationProblem::createPattern))
      .def("createPattern", py::overload_cast<const Sophus::SE3d &>(
                                &SystemCalibrationProblem::createPattern))
      .def("createFlatPattern",
           py::overload_cast<std::shared_ptr<Pattern> &,
                             const FlatPattern::PatternVector &>(
               &SystemCalibrationProblem::createFlatPattern))
      .def("createFlatPattern",
           py::overload_cast<std::shared_ptr<Pattern> &>(
               &SystemCalibrationProblem::createFlatPattern))
      .def("createFlatPattern",
           py::overload_cast<std::shared_ptr<Pattern> &,
                             const FlatPattern::PatternVector &,
                             const Sophus::SE3d &>(
               &SystemCalibrationProblem::createFlatPattern))
      .def("createCamera",
           py::overload_cast<const std::shared_ptr<CameraModel> &>(
               &SystemCalibrationProblem::createCamera))
      .def("createCamera",
           py::overload_cast<const std::shared_ptr<CameraModel> &,
                             const Sophus::SE3d &>(
               &SystemCalibrationProblem::createCamera))
      .def("createSystem", &SystemCalibrationProblem::createSystem)
      .def("addObservation",
           py::overload_cast<const std::shared_ptr<Camera> &,
                             const std::shared_ptr<PositionedSystem> &,
                             const std::shared_ptr<FlatPattern> &>(
               &SystemCalibrationProblem::addObservation))
      .def("addObservation", py::overload_cast<systemConnection &>(
                                 &SystemCalibrationProblem::addObservation))
      .def("calibrate", &SystemCalibrationProblem::calibrate);
}
