# Multi Camera Calibration Toolkit

This Project is C++ library for calibrating multi-camera systems with different types of camera models.

__Supported camera models:__  
1. Pinhole with radial and tangential distortion model   
2. Atan-fisheye camera model  
3. Omnidirectional camera model  

__Dependencies:__  
1. [ceres-solver](https://github.com/ceres-solver/ceres-solver)  
2. [Eigen](https://bitbucket.org/eigen/eigen/)  
3. [Sophus](https://github.com/strasdat/Sophus)  

## Build

```
git clone https://bitbucket.org/supervised_calibration/supervised_calibration
git checkout dsinitsyn_atan_calibration_implementation
git submodule init
git submodule update
mkdir build
cd build
cmake ../ -DBUILS_DOC=true -DBUILD_WRAPPERS=true -DCMAKE_BUILD_TYPE=Release
make 
```

